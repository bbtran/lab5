/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#include "Camera.h"
#include <GL/glut.h>
#include "3dclasses.h"
#include "CollisionTester.h"
#include "Statistics.h"
#include "CGWorld.h"
#include "util.h"

Camera::Camera(CGWorld *pWorld): _pWorld(pWorld), _pCollisionTester(new CollisionTester(pWorld, CAMERA_RADIUS_X, CAMERA_RADIUS_Y, CAMERA_RADIUS_Z)),
	_position(Vertex3(0, 0, 0)), _yaw(0), _pitch(0), _fallingSpeed(0.0), _lastUpdate(-1.0)
{
}

Camera::~Camera(void)
{
	delete _pCollisionTester;
}

void Camera::look(void)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	// Setup projection
	float aspectRatio = static_cast<float>(glutGet(GLUT_WINDOW_WIDTH)) /
						static_cast<float>(glutGet(GLUT_WINDOW_HEIGHT));

	gluPerspective(CAMERA_FOV, aspectRatio, CAMERA_NEAR, CAMERA_FAR);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Rotate and translate the world
	glRotatef(-_pitch, 1.0, 0.0, 0.0);
	glRotatef(-_yaw,   0.0, 1.0, 0.0);
	glTranslatef(-_position.x(), -_position.y(), -_position.z());
}

void Camera::lookInDirection(void)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	// Setup projection
	float aspectRatio = static_cast<float>(glutGet(GLUT_WINDOW_WIDTH)) /
						static_cast<float>(glutGet(GLUT_WINDOW_HEIGHT));

	gluPerspective(CAMERA_FOV, aspectRatio, CAMERA_NEAR, CAMERA_FAR); 

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Only rotate the world, ignore position
	glRotatef(-_pitch, 1.0, 0.0, 0.0);
	glRotatef(-_yaw,   0.0, 1.0, 0.0);
}

void Camera::move(Vector3 velocity)
{
	// Shift the camera point to get the collision ellipsoid's center
	Vertex3 collisionPos = _position - CAMERA_SHIFT_VEC;
	
	// Check for collisions and get a new position
	collisionPos = _pCollisionTester->collide(collisionPos, velocity);
	
	// Shift back again
	_position = collisionPos + CAMERA_SHIFT_VEC;

	// Update statistics
	Statistics::instance().triangleCollisions += _pCollisionTester->getLastTriangleCount();
}

void Camera::update(float walk, float strafe)
{
	// Get the time passed since the last update
	double time = _pWorld->getTimer().getTime();
	if (_lastUpdate < 0)
		_lastUpdate = time;

	double timePassed = time - _lastUpdate;

	// Update the falling speed by the acceleration
	_fallingSpeed += timePassed * CAMERA_GRAVITY;

	// Check the falling speed boundaries
	if (_fallingSpeed < CAMERA_MIN_GRAVITY_SPEED)
		_fallingSpeed = CAMERA_MIN_GRAVITY_SPEED;
	if (_fallingSpeed > CAMERA_MAX_GRAVITY_SPEED)
		_fallingSpeed = CAMERA_MAX_GRAVITY_SPEED;

	// Calculate the yaw angles of the walking and strafe directions
	float radYawWalk = deg2rad(_yaw);
	float radYawStrafe = deg2rad(_yaw - 90);
	
	// Setup vectors for walk and strafe
	Vector3 walkDirection(-sin(radYawWalk), 0.0f, -cos(radYawWalk));
	Vector3 strafeDirection(-sin(radYawStrafe), 0.0f, -cos(radYawStrafe));

	// Build the velocity vector
	Vector3 velocity = walkDirection * walk + strafeDirection * strafe;

	// Add some gravity
	velocity += Vector3(0.0f, -timePassed * _fallingSpeed, 0.0f);

	// Save old position and move
	Vertex3 oldPos = _position;
	move(velocity);

	if (timePassed > 0.0f)
	{
		// Set the falling speed to how much we have moved 
		// in the y-direction. 
		_fallingSpeed = oldPos.y() - _position.y();
		_fallingSpeed /= timePassed;

		// Do not count upwards movement
		if (_fallingSpeed < 0.0f) _fallingSpeed = 0.0f;
	}

	_lastUpdate = time;
}

void Camera::yaw(float angle)
{
	_yaw += angle;
	_yaw = fmod(_yaw, 360.0f);
}

void Camera::pitch(float angle)
{
	_pitch += angle;
	if (_pitch > MAX_PITCH) _pitch = MAX_PITCH;
	if (_pitch < MIN_PITCH) _pitch = MIN_PITCH;
}

Vertex3 Camera::getPosition(void) const
{
	return _position;
}


void Camera::setPosition(const Vertex3 &pos)
{
	_position = pos;
}