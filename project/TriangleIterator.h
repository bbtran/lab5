/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#ifndef _CG_TRIANGLEITERATOR_H_
#define _CG_TRIANGLEITERATOR_H_

class Triangle;

// Abstract base class for an iterator of triangles
class TriangleIterator
{
public:
	/* Moves to the next triangle. Returns true if there is still
	   a triangle, or false if all have been iterated over. Initially, 
	   the iterator is placed before the first triangle, so a next() call
	   should be done to get the first triangle. */
	virtual bool next() = 0;

	// Returns a const reference to the current triangle
	virtual const Triangle & current() = 0;

	// Destructor
	virtual ~TriangleIterator(void) {};
};

#endif
