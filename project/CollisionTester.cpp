/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

// This warning is disabled because of a bug in VC6.
// It supresses warnings about too long debug symbols 
#pragma warning(disable: 4786)

#include <iostream>
#include <map>


#include "CollisionTester.h"
#include "CGWorld.h"
#include "NodeSelector.h"

/* NOTE:

	Most of the code in this class was based on the collision detection papers:
	"Improved Collision Detection and Response", Kasper Fauerby (Peroxide)
	"Generic Collision Detection for Games using Ellipsoids", Paul Nettle (Fluid Studios)
	
	They can be found here:
	http://www.peroxide.dk/papers/collision/collision.pdf
	http://www.fluidstudios.com/pub/FluidStudios/CollisionDetection/Fluid_Studios_Generic_Collision_Detection_for_Games_Using_Ellipsoids.pdf

*/

CollisionTester::CollisionTester(CGWorld *pWorld, float rx, float ry, float rz):
	_pWorld(pWorld), _scaleX(1.0 / rx), _scaleY(1.0 / ry), _scaleZ(1.0 / rz)
{
}

CollisionTester::~CollisionTester(void)
{
}


bool CollisionTester::getLowestPositiveRoot(float a, float b, float c, float *x)
{
	// ABC formula
	float d = b * b - 4.0 * a * c; // d = b^2 - 4ac
	if (d < 0.0) return false; // no solutions

	float dSquare = sqrt(d);

	float x1 = (-b - dSquare) / (2 * a);
	float x2 = (-b + dSquare) / (2 * a);

	// x1 should be smallest
	if (x1 > x2) 
		swapVars(x1, x2);
	
	if (x1 > 0.0)
	{
		*x = x1;
		return true;
	}
	else if (x2 > 0.0)
	{
		*x = x2;
		return true;
	}

	return false;
}

// On collision, in pT the collision time is returned, and pCollisionPoint is set. 
// Returns true if a collision occured, false otherwise
inline bool CollisionTester::sweepSinglePoint(const Vertex3 &sourcePoint,
											  const Vector3 &velocity,
											  const Vertex3 &testPoint,
											  Vertex3 *pCollisionPoint,
											  float *pT)
{
	float a, b, c;

	// Setup a, b and c values for the equation
	a = velocity.squaredLength();
	b = 2.0 * velocity.dot(sourcePoint - testPoint);
	c = (testPoint - sourcePoint).squaredLength() - 1.0;

	float x;
	if (getLowestPositiveRoot(a, b, c, &x))
	{
		// Is it before the time we had?
		if (x < *pT)
		{
			*pT = x;
			*pCollisionPoint = testPoint;
			return true;
		}
	}
	return false;
}

// On collision, in pT the collision time is returned, and pCollisionPoint is set. 
// Returns true if a collision occured, false otherwise
bool CollisionTester::sweepPoints(const Vertex3 &sourcePoint,
								  const Vector3 &velocity, 
								  const Triangle &poly, 
								  Vertex3 *pCollisionPoint, 
								  float *pT)
{
	bool found = false;

	// Test all points
	found |= sweepSinglePoint(sourcePoint, velocity, poly.v1(), pCollisionPoint, pT);
	found |= sweepSinglePoint(sourcePoint, velocity, poly.v1(), pCollisionPoint, pT);
	found |= sweepSinglePoint(sourcePoint, velocity, poly.v3(), pCollisionPoint, pT);
	
	return found;
}

// On collision, in pT the collision time is returned, and pCollisionPoint is set. 
// Returns true if a collision occured, false otherwise
bool CollisionTester::sweepSingleEdge(const Vertex3 &sourcePoint,
									 const Vector3 &velocity,
									 const Ray &edge,
									 Vertex3 *pCollisionPoint,
									 float *pT)
{
	Vector3 sourceToBase = edge.origin() - sourcePoint;
	float edgeSquaredLength = edge.vector().squaredLength();
	float edgeDotVelocity = edge.vector().dot(velocity);
	float edgeDotSourceToBase = edge.vector().dot(sourceToBase);
	
	// Setup a, b and c values of the equation
	float a = edgeSquaredLength * - velocity.squaredLength() + edgeDotVelocity * edgeDotVelocity;
	float b = edgeSquaredLength * (2.0 * velocity.dot(sourceToBase)) - 2.0 * edgeDotVelocity * edgeDotSourceToBase;
	float c = edgeSquaredLength * (1.0 - sourceToBase.squaredLength()) + edgeDotSourceToBase * edgeDotSourceToBase;

	float x;
	if (getLowestPositiveRoot(a, b, c, &x))
	{
		float f = (edgeDotVelocity * x  - edgeDotSourceToBase) / edgeSquaredLength;
		
		// Is it on the edge?
		if (f >= 0.0 && f <= 1.0)
		{
			// Is it before the time we already had?
			if (x < *pT)
			{
				*pT = x;
				*pCollisionPoint = edge.origin() + edge.vector() * f;
				return true;
			}
		}
	}
	return false;
}

// On collision, in pT the collision time is returned, and pCollisionPoint is set. 
// Returns true if a collision occured, false otherwise
bool CollisionTester::sweepEdges(const Vertex3 &sourcePoint,
								 const Vector3 &velocity, 
								 const Triangle &poly, 
								 Vertex3 *pCollisionPoint, 
								 float *pT)
{
	bool found = false;

	// Test all three edges
	found |= sweepSingleEdge(sourcePoint, velocity, Ray(poly.v1(), poly.v2() - poly.v1()), pCollisionPoint, pT);
	found |= sweepSingleEdge(sourcePoint, velocity, Ray(poly.v2(), poly.v3() - poly.v2()), pCollisionPoint, pT);
	found |= sweepSingleEdge(sourcePoint, velocity, Ray(poly.v3(), poly.v1() - poly.v3()), pCollisionPoint, pT);
	
	return found;
}

// Test for a collision. pCol->sourcePoint should be set, as well as velocity and boundingBox
void CollisionTester::testCollision(Collision *pCol)
{
	const Vertex3 sourcePoint = pCol->sourcePoint;
	const Vector3 velocity = pCol->velocity;

	// Setup initial values
	bool collisionFound = false;
	float collisionTime = -1.0;
	Vertex3 nearestIntersectionPoint;
	pCol->stuck = false;
	
	// Setup a selector based on the bounding box given in pCol
	BoundingBoxSelector selector(pCol->boundingBox);

	// Iterate over all faces within the bounding box
	CGFaceTreeIterator it = _pWorld->getIterator(&selector); 
	while(it.next())
	{
		// Get a polygon
		Triangle poly = it.current();
		_lastTriangleCount++;

		// Scale to ellipsoid space
		poly.scaleWorld(_scaleX, _scaleY, _scaleZ);

		// Get the plane through the polygon
		Plane polyPlane = poly.plane();
		
		// Intersection is between time t0 and t1, t is the interseciton time.
		double t0, t1;
		float t = 1.0;
		bool embeddedSphere = false;
		
		// Determine distance to plane
		double planeDistance = polyPlane.distance(sourcePoint);
		
		Vertex3 collisionPoint;
		bool curPolyCollision = false;

		// Ignore backfacing:
		if (planeDistance < 0.0) continue;		
		
		float normalDotVelocity = polyPlane.normal().dot(velocity); 
		
		if (normalDotVelocity == 0.0) 
		{
			// Sphere is travelling parallel
			if (fabs(planeDistance) >= 1.0)
				continue; // Sphere is not embedded, continue
			
			// Sphere is embedded
			embeddedSphere = true;
			t0 = 0.0;
			t1 = 1.0; // Intersects between the whole range (0 to 1)
		}
		else
		{
			// Intersects somewhere
			t0 = (-1.0 - planeDistance) / normalDotVelocity;
			t1 = (+1.0 - planeDistance) / normalDotVelocity;

			// t0 should be the smallest:
			if (t0 > t1)
				swapVars(t0, t1);

			if (t0 > 1.0 || t1 < 0.0)
				continue; // Sphere does not pass through poygon
			clamp01(t0);
			clamp01(t1);
		}

		// Check if the sphere is not embedded
		if (!embeddedSphere)
		{
			// Get the plane intersection point
			Vertex3 planeIntersectionPoint = (sourcePoint - polyPlane.normal()) + velocity * t0;

			// Check if this point is inside the triangle
			if (poly.inside(planeIntersectionPoint))
			{
				// The sphere collides with the inside of the triangle
				curPolyCollision = true; 
				t = t0;
				collisionPoint = planeIntersectionPoint;
				
				// If the distance < 1.0, we are stuck
				if (planeDistance < 1.0f)
				{
					pCol->stuck = true;
				}
			}
		}
		
		// If no collision is found yet..
		if (!curPolyCollision)
		{
			// A sweep against the points and edges of the polygon is needed:
			curPolyCollision |= sweepPoints(sourcePoint, velocity, poly, &collisionPoint, &t); 
			curPolyCollision |= sweepEdges(sourcePoint, velocity, poly, &collisionPoint, &t); 
		}

		// Did we found a collision?
		if (curPolyCollision)
		{
			// If we found one earlier check if this one is nearer
			if (!collisionFound || t < collisionTime)
			{
				collisionFound = true;
				collisionTime = t;
				nearestIntersectionPoint = collisionPoint;
			}
		}
	}
	pCol->collisionFound = collisionFound;
	pCol->nearestDistance = collisionTime * velocity.length();
	pCol->intersectionPoint = nearestIntersectionPoint;

	
	// If we collided, do an additional stuck-check.
	if (collisionFound)
	{
		if (pCol->intersectionPoint.distance(pCol->sourcePoint) <= 1.0000)
		{
			pCol->stuck = true;
		}
	}
}

Vertex3 CollisionTester::collide(Vertex3 source, Vector3 velocity)
{
	// The collision data
	Collision col;

	// Get a bounding box around the triangles we might collide with
	Vector3 scaleVec(2.0 / _scaleX, 2.0 / _scaleY, 2.0 / _scaleZ);
	col.boundingBox = BoundingBox(source - scaleVec, source + scaleVec);
	col.boundingBox.include(source + velocity - scaleVec);
	col.boundingBox.include(source + velocity + scaleVec);

	int step = 0;

	// Scale the source and velocity to ellipsoid space
	source.scaleWorld(_scaleX, _scaleY, _scaleZ); 
	velocity.scaleWorld(_scaleX, _scaleY, _scaleZ);
	
	_lastTriangleCount = 0;

	float veryCloseDistance = COLLISION_EPSILON; 

	// Iteratively collide:
	for(step=0;step<COLLISION_MAX_STEPS;step++)
	{
		// Ignore very small moves:
		if (velocity.length() < 2* COLLISION_EPSILON) break;  

		// Setup new collision data:
		col.sourcePoint = source;
		col.velocity = velocity;

		// Test for collision
		testCollision(&col);

		// If we are stuck, return to the last valid position
		if (col.stuck)
		{
			source = _lastValidPosition;
			col.sourcePoint = _lastValidPosition;
			continue;
		}
		else
		{
			// We are not stuck, save the source position
			_lastValidPosition = source;
		}

		// No collision? Just move to the new point
		if (col.collisionFound == false) 
		{
			source += velocity;
			break;
		}
		

		// A collision occured
	
		// Calcualte the would-be distination point
		Vertex3 destinationPoint = source + velocity;
		
		Vertex3 newSourcePoint = source;
		
		
		
		//If we are at a zero distance, do not move
		if(col.nearestDistance == 0)
		{
			break; 
		}
	
		// Copy velocity vector, resized to nearest distance
		Vector3 v = velocity.resized(col.nearestDistance);
		newSourcePoint = col.sourcePoint + v;

		// Determine the sliding plane
		Vector3 slidePlaneNormal = newSourcePoint - col.intersectionPoint;
		slidePlaneNormal.normalize();


		// Determine the displacement vector
		float factor;
		float dot = v.dot(slidePlaneNormal); // V projected on the slide plane normal
		
		if (dot==0)
			factor = 0;
		else
            factor = veryCloseDistance / dot; 
		
		// Make sure the factor is not too big
		if (factor > col.nearestDistance)
			factor = col.nearestDistance;

		Vector3 displacementVector = v * factor;

		// The displacement vector will move the position so that it will
		// have a distance 'veryCloseDistance' to the polygon.
		
		// Add the displacement vector
		newSourcePoint = newSourcePoint + displacementVector;

		// Update the intersection point
		col.intersectionPoint = col.intersectionPoint + displacementVector;

		// Calulate a slide plane
		Vertex3 slidePlaneOrigin = col.intersectionPoint;	
		Plane slidingPlane(slidePlaneOrigin,slidePlaneNormal);
		
		// Update destination point
		Vertex3 newDestinationPoint = destinationPoint - slidePlaneNormal * slidingPlane.distance(destinationPoint);
		
		// Get a new velocity vector
		Vector3 newVelocityVector = newDestinationPoint - col.intersectionPoint;

		// If the distance is small, ignore:
		if (newVelocityVector.length() <= veryCloseDistance) 
		{
			break;
		}

		// Update source and velocity for the next round
		source = newSourcePoint;
		velocity = newVelocityVector;
	}
    
	// Scale back the source point (which is now the new destination point)
	source.scaleWorld(1.0 / _scaleX, 1.0 / _scaleY, 1.0 / _scaleZ);
	return source;
}

int CollisionTester::getLastTriangleCount() const
{
	return _lastTriangleCount;
}