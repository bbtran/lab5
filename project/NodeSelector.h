/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#ifndef _CG_NODESELECTOR_H_
#define _CG_NODESELECTOR_H_

#include "BoundingBox.h"

/* Abstract class to select nodes based on their bounding boxes */
class NodeSelector
{
public:
	/* This method should return true if the node given in the parameter should
	   be selected */
	virtual bool includes(const BoundingBox &node) const = 0;
	virtual ~NodeSelector(void) {}
};

/* The bounding box selector implements the NodeSelcetor class. It will select
   a node if its bounding box intersects with a bounding box given at construction
   time. */
class BoundingBoxSelector : public NodeSelector
{
public:
	// In the constructor a bounding box can be given to test intersections with
	BoundingBoxSelector(BoundingBox box): _box(box)
	{ 
	}
	
	virtual ~BoundingBoxSelector() 
	{
	}

	virtual bool includes(const BoundingBox &node) const 
	{ 
		return _box.intersects(node); 
	}

private:
	BoundingBox _box;
};

#endif

