/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#include "SkyMap.h"

SkyMap::SkyMap(void)
{
	for (int i=0;i<6;i++)
	{
		_textures[i] = new CGTexture(SKYMAP_FILENAMES[i]);
		_textures[i]->loadTextureFile(); 
	}

}

SkyMap::~SkyMap(void)
{
	for (int i=0;i<6;i++)
		delete _textures[i];
}

void SkyMap::draw(void)
{
	glDisable(GL_LIGHTING);
	glEnable(GL_TEXTURE_2D);
	glDisable(GL_CULL_FACE);
	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glPushMatrix();


		// Draw faces
		
			// 6 faces
			for (int i=0;i<6;i++)
			{
				_textures[i]->activate();
				
				// Setup color and material
				glColor3f(1,1,1);
				// 4 points per face
				glBegin(GL_QUADS);
				for (int j=0;j<4;j++)
				{
					int verCornerIndex = SKYMAP_FACE_CORNER_IDX[i][j];
					int texCornerIndex = SKYMAP_TEX_CORNER_IDX[i][j];

					glTexCoord2fv(SKYMAP_TEX_COORDS[texCornerIndex]);
					glVertex3fv(SKYMAP_CORNERS[verCornerIndex]);
				}
				glEnd();	
			}
		
	glPopMatrix();
}
