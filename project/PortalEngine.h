/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#ifndef _CG_PORTALENGINE_H_
#define _CG_PORTALENGINE_H_

#include "3dclasses.h"
#include "NodeSelector.h"
#include "PlaneClipper.h"
#include "PortalNodeSelector.h"

class CGWorld;

// The portal engine
class PortalEngine
{
public:
	PortalEngine(CGWorld *pWorld);
	virtual ~PortalEngine(void);
	
	// Get a selector with the given frustum
	PortalNodeSelector getSelector(const Frustum &frustum);
	
private:
	// Internal functions to check all rooms for visibility
	void recurseRooms(int roomID, 
					  PortalNodeSelector *pSelector,
					  int *pVisitedPortals,
					  int visitedPortalsCount,
					  const Vertex3 &cameraPos,
					  PlaneClipper visiblePart,
					  const Frustum &frustum);
	CGWorld *_pWorld;

};


#endif

