/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#include "Frustum.h"
#include "BoundingBox.h"
#include <GL/glut.h>

/* This Frustum class is based largely on the code from the Frustum Culling tutorial
   on gametutorials.com:
   
   http://www.gametutorials.com/Tutorials/opengl/OpenGL_Pg5.htm

*/

Frustum::Frustum(void)
{
}

Frustum::~Frustum(void)
{
}

void Frustum::calculateFrustum(void)
{
	// Note: this function has been copied largely from the site mentioned at the top of this file

	float   proj[16];	// Projection matrix
	float   modl[16];	// Model-view matrix
	float   clip[16];	// Clipping planes


	// Get matrices
	glGetFloatv( GL_PROJECTION_MATRIX, proj );
	glGetFloatv( GL_MODELVIEW_MATRIX, modl );


	// Combine two matrices

	clip[ 0] = modl[ 0] * proj[ 0] + modl[ 1] * proj[ 4] + modl[ 2] * proj[ 8] + modl[ 3] * proj[12];
	clip[ 1] = modl[ 0] * proj[ 1] + modl[ 1] * proj[ 5] + modl[ 2] * proj[ 9] + modl[ 3] * proj[13];
	clip[ 2] = modl[ 0] * proj[ 2] + modl[ 1] * proj[ 6] + modl[ 2] * proj[10] + modl[ 3] * proj[14];
	clip[ 3] = modl[ 0] * proj[ 3] + modl[ 1] * proj[ 7] + modl[ 2] * proj[11] + modl[ 3] * proj[15];

	clip[ 4] = modl[ 4] * proj[ 0] + modl[ 5] * proj[ 4] + modl[ 6] * proj[ 8] + modl[ 7] * proj[12];
	clip[ 5] = modl[ 4] * proj[ 1] + modl[ 5] * proj[ 5] + modl[ 6] * proj[ 9] + modl[ 7] * proj[13];
	clip[ 6] = modl[ 4] * proj[ 2] + modl[ 5] * proj[ 6] + modl[ 6] * proj[10] + modl[ 7] * proj[14];
	clip[ 7] = modl[ 4] * proj[ 3] + modl[ 5] * proj[ 7] + modl[ 6] * proj[11] + modl[ 7] * proj[15];

	clip[ 8] = modl[ 8] * proj[ 0] + modl[ 9] * proj[ 4] + modl[10] * proj[ 8] + modl[11] * proj[12];
	clip[ 9] = modl[ 8] * proj[ 1] + modl[ 9] * proj[ 5] + modl[10] * proj[ 9] + modl[11] * proj[13];
	clip[10] = modl[ 8] * proj[ 2] + modl[ 9] * proj[ 6] + modl[10] * proj[10] + modl[11] * proj[14];
	clip[11] = modl[ 8] * proj[ 3] + modl[ 9] * proj[ 7] + modl[10] * proj[11] + modl[11] * proj[15];

	clip[12] = modl[12] * proj[ 0] + modl[13] * proj[ 4] + modl[14] * proj[ 8] + modl[15] * proj[12];
	clip[13] = modl[12] * proj[ 1] + modl[13] * proj[ 5] + modl[14] * proj[ 9] + modl[15] * proj[13];
	clip[14] = modl[12] * proj[ 2] + modl[13] * proj[ 6] + modl[14] * proj[10] + modl[15] * proj[14];
	clip[15] = modl[12] * proj[ 3] + modl[13] * proj[ 7] + modl[14] * proj[11] + modl[15] * proj[15];
	

	// This will extract the RIGHT side of the frustum
	_frustum[RIGHT][A] = clip[ 3] - clip[ 0];
	_frustum[RIGHT][B] = clip[ 7] - clip[ 4];
	_frustum[RIGHT][C] = clip[11] - clip[ 8];
	_frustum[RIGHT][D] = clip[15] - clip[12];
	normalizePlane(RIGHT);

	// This will extract the LEFT side of the frustum
	_frustum[LEFT][A] = clip[ 3] + clip[ 0];
	_frustum[LEFT][B] = clip[ 7] + clip[ 4];
	_frustum[LEFT][C] = clip[11] + clip[ 8];
	_frustum[LEFT][D] = clip[15] + clip[12];
	normalizePlane(LEFT);

	// This will extract the BOTTOM side of the frustum
	_frustum[BOTTOM][A] = clip[ 3] + clip[ 1];
	_frustum[BOTTOM][B] = clip[ 7] + clip[ 5];
	_frustum[BOTTOM][C] = clip[11] + clip[ 9];
	_frustum[BOTTOM][D] = clip[15] + clip[13];
	normalizePlane(BOTTOM);

	// This will extract the TOP side of the frustum
	_frustum[TOP][A] = clip[ 3] - clip[ 1];
	_frustum[TOP][B] = clip[ 7] - clip[ 5];
	_frustum[TOP][C] = clip[11] - clip[ 9];
	_frustum[TOP][D] = clip[15] - clip[13];
	normalizePlane(TOP);

	// This will extract the BACK side of the frustum
	_frustum[BACK][A] = clip[ 3] - clip[ 2];
	_frustum[BACK][B] = clip[ 7] - clip[ 6];
	_frustum[BACK][C] = clip[11] - clip[10];
	_frustum[BACK][D] = clip[15] - clip[14];
	normalizePlane(BACK);

	// This will extract the FRONT side of the frustum
	_frustum[FRONT][A] = clip[ 3] + clip[ 2];
	_frustum[FRONT][B] = clip[ 7] + clip[ 6];
	_frustum[FRONT][C] = clip[11] + clip[10];
	_frustum[FRONT][D] = clip[15] + clip[14];
	normalizePlane(FRONT);
}

void Frustum::normalizePlane(int side)
{
	// Note: this function has been copied largely from the site mentioned at the top of this file

	// Here we calculate the magnitude of the normal to the plane (point A B C)
	// Remember that (A, B, C) is that same thing as the normal's (X, Y, Z).
	// To calculate magnitude you use the equation:  magnitude = sqrt( x^2 + y^2 + z^2)
	float length  = (float)sqrt(	_frustum[side][A] * _frustum[side][A] + 
									_frustum[side][B] * _frustum[side][B] + 
									_frustum[side][C] * _frustum[side][C] );

	// Then we divide the plane's values by it's magnitude.
	// This makes it easier to work with.
	_frustum[side][A] /= length;
	_frustum[side][B] /= length;
	_frustum[side][C] /= length;
	_frustum[side][D] /= length; 
}

bool Frustum::boxInFrustum(const BoundingBox &box) const
{
	// Note: this function has been copied largely from the site mentioned at the top of this file

	Vertex3 minPoint = box.minPoint();
	Vertex3 maxPoint = box.maxPoint();
	float x1 = minPoint.x(), y1 = minPoint.y(), z1 = minPoint.z();
	float x2 = maxPoint.x(), y2 = maxPoint.y(), z2 = maxPoint.z();

	for(int i = 0; i < 6; i++ )
	{
		if(_frustum[i][A] * (x1) + _frustum[i][B] * (y1) + _frustum[i][C] * (z1) + _frustum[i][D] > 0)
		continue;
		if(_frustum[i][A] * (x2) + _frustum[i][B] * (y1) + _frustum[i][C] * (z1) + _frustum[i][D] > 0)
		continue;
		if(_frustum[i][A] * (x1) + _frustum[i][B] * (y2) + _frustum[i][C] * (z1) + _frustum[i][D] > 0)
		continue;
		if(_frustum[i][A] * (x2) + _frustum[i][B] * (y2) + _frustum[i][C] * (z1) + _frustum[i][D] > 0)
		continue;
		if(_frustum[i][A] * (x1) + _frustum[i][B] * (y1) + _frustum[i][C] * (z2) + _frustum[i][D] > 0)
		continue;
		if(_frustum[i][A] * (x2) + _frustum[i][B] * (y1) + _frustum[i][C] * (z2) + _frustum[i][D] > 0)
		continue;
		if(_frustum[i][A] * (x1) + _frustum[i][B] * (y2) + _frustum[i][C] * (z2) + _frustum[i][D] > 0)
		continue;
		if(_frustum[i][A] * (x2) + _frustum[i][B] * (y2) + _frustum[i][C] * (z2) + _frustum[i][D] > 0)
		continue;

		// If we get here, it isn't in the frustum
		return false;
	}

	return true;
}
