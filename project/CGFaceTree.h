/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#ifndef _CG_CGFACETREE_H_
#define _CG_CGFACETREE_H_

#include "AutoArr.h"
#include "3dclasses.h"
// This warning is disabled because of a bug in VC6.
// It supresses warnings about too long debug symbols 
#pragma warning (disable: 4786)
#include <vector>
#include <iostream>
#include "DataFile.h"
#include <GL/glut.h>

#include "Camera.h"
#include "CGFaceID.h"

#include "DisallowCopy.h"

class CGRoom;
class CGWorld;

// Represents one node in the face tree
struct CGFaceTreeNode
{
	// Initialize
	CGFaceTreeNode()
	{
		used = false;
		box = BoundingBox(Vertex3(0), Vertex3(0));
		faceIDIndex = 0; 
		faceCount = 0;
	}
	
	// (Un)serialize the node
	void exchange(DataFile *pFile)
	{
		pFile->exchange(&used);
		box.exchange(pFile);
		pFile->exchange(&faceIDIndex);
		pFile->exchange(&faceCount);
	}
	
	bool		used;			// Node in use or not?
	BoundingBox box;			// Bounding box of this node
	int			faceIDIndex;	// Start index in face ID list 
	int			faceCount;		// Number of faces belonging to this node
};

// The AABB tree with all faces in the static world.
class CGFaceTree : private DisallowCopy
{
public:
	CGFaceTree();
	virtual ~CGFaceTree();

	// Sets the list with face IDs
	void setFaceIDList(CGFaceID *pList, int count);
	
	// Creates an new node. Parent is the parent index, left tells whether it should be 
	// a left (true) or right (false) child of the parent.
	int  newNode(int parent, bool left);

	// Set the properties of the node at idex idx.
	void setNode(int idx, BoundingBox box, int faceIDIndex, int faceCount);
	
	// (Un)serialize the tree
	void exchange(DataFile *pFile);
	
	// Get face ID from the list at index
	const CGFaceID * getFaceIDPtr(int index) const
	{
		return _faceIDList.get() +  index;
	}

	// Get a node
	CGFaceTreeNode getNode(int index) const 
	{
		return _nodeList[index];
	}

	// Return the number of nodes
	int getNodeCount() const
	{
		return static_cast<int>(_nodeList.size());
	}

private:
	// Set node to 'in use' state
	void useNodeIndex(int idx);

	// List of all nodes, stored as flat array. If a node is at 
	// index i, it's childs are at 2i + 1 and 2i + 2
	std::vector<CGFaceTreeNode>	_nodeList;		
	AutoArr<CGFaceID>			_faceIDList;	// List of all face IDs
};


#endif

