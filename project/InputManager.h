/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#ifndef _CG_INPUTMANAGER_H_
#define _CG_INPUTMANAGER_H_

class CGWorld;

const float INPUT_MANAGER_WALK_SPEED = 160.0;	// In units per second
const float INPUT_MANAGER_MOUSE_FACTOR = 0.5f;	// In degrees per pixel
// Dispatches user input events.
class InputManager
{
public:
	InputManager(CGWorld *pWorld);
	virtual ~InputManager(void);
	
	// Handles the mouse events from GLUT
	void onButton(int button, int state, int x, int y);
	void onMotion(bool active, int x, int y);
	void onKey(unsigned char key, int x, int y);
	void onKeyUp(unsigned char key, int x, int y);
	void onSpecialKey(int key, int x, int y);
	void onSpecialKeyUp(int key, int x, int y);

	// Should be called on every frame
	void onFrame();

	// Returns true if the user wants stats
	bool shouldShowStats() const { return _showStats; };
private:
	// Are we looking with the mouse?
	bool	_looking;		
	
	// The world
	CGWorld	*_pWorld;		

	// Previous mouse position:
	int		_prevX;
	int		_prevY;

	// Booleans to tell if the up/down/left/right keys are pressed:
	bool	_upKey;
	bool	_downKey;
	bool	_leftKey;
	bool	_rightKey;

	// Show statistics?
	bool	_showStats;

	// Time the last frame was rendererd
	double	_lastFrameTime;

};

#endif
