/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#pragma warning (disable: 4786)
#include "CGFaceTree.h"
#include "CGWorld.h"
#include "Camera.h"

#include <iostream>
// This warning is disabled because of a bug in VC6.
// It supresses warnings about too long debug symbols 
#pragma warning (disable: 4786)
#include <map>

CGFaceTree::CGFaceTree()
{
	useNodeIndex(0);
}

CGFaceTree::~CGFaceTree()
{

}

int CGFaceTree::newNode(int parent, bool left)
{
	// Create a new node
	int newIdx = parent * 2 + (left ? 1 : 2);
    useNodeIndex(newIdx);
	return newIdx;
}

void CGFaceTree::setNode(int idx, BoundingBox box, int faceIDIndex, int faceCount)
{
	// Set node in used state and set properties
	useNodeIndex(idx);
	CGFaceTreeNode &newNode = _nodeList[idx];
	newNode.box = box;
	newNode.faceIDIndex = faceIDIndex;
	newNode.faceCount = faceCount;
}

void CGFaceTree::setFaceIDList(CGFaceID *pList, int count)
{
	// Copy the face ID list
	_faceIDList.alloc(count);
	memcpy(_faceIDList.get(), pList, count * sizeof(CGFaceID));
}

void CGFaceTree::useNodeIndex(int idx)
{
	// Find node
	int size = static_cast<int>(_nodeList.size());
	if (idx >= size)
		_nodeList.resize(idx + 1);

	// In use
	_nodeList[idx].used = true;
}

void CGFaceTree::exchange(DataFile *pFile)
{
	_faceIDList.exchange(pFile);

	int	count = _nodeList.size();
	pFile->exchange(&count);

	if (!pFile->writing()) 
	{
		_nodeList.resize(count);
	}
	for (int i=0;i<count;i++)
	{
		_nodeList[i].exchange(pFile);
	}
}
