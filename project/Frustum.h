/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#ifndef _CG_FRUSTUM_
#define _CG_FRUSTUM_

/* This Frustum class is based largely on the code from the Frustum Culling tutorial
   on gametutorials.com:
   
   http://www.gametutorials.com/Tutorials/opengl/OpenGL_Pg5.htm

*/

class BoundingBox;

class Frustum
{
public:
	Frustum(void);
	virtual ~Frustum(void);
	
	// Extract the frustum from openGL
	void calculateFrustum(void);
	// Returns true if a box is inside the frustum
	bool Frustum::boxInFrustum(const BoundingBox &box) const;
private:
	void normalizePlane(int side);

	// Indices for each frustum side
	enum FrustumSide
	{
		RIGHT	= 0,		// The right side of the frustum
		LEFT	= 1,		// The left	 side of the frustum
		BOTTOM	= 2,		// The bottom side of the frustum
		TOP		= 3,		// The top side of the frustum
		BACK	= 4,		// The back	side of the frustum
		FRONT	= 5			// The front side of the frustum
	}; 

	// Indices for the four values that define a plane
	enum PlaneData
	{
		A = 0,				// The X value of the plane's normal
		B = 1,				// The Y value of the plane's normal
		C = 2,				// The Z value of the plane's normal
		D = 3				// The distance the plane is from the origin
	};
	
	// This holds the A B C and D values for each side of our frustum.
	float _frustum[6][4];

};

#endif
