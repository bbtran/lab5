/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#ifndef _CG_3DCLASSES_H_
#define _CG_3DCLASSES_H_

// This warning is disabled because of a bug in VC6.
// It supresses warnings about too long debug symbols 
#pragma warning (disable: 4786)


// This header simply includes most of the 3D base classes

#include "Vector.h"
#include "Vertex.h"
#include "Ray.h"
#include "Plane.h"
#include "Triangle.h"
#include "BoundingBox.h"

#endif

