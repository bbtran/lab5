/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#ifndef _CG_CGWORLD_H_
#define _CG_CGWORLD_H_

// This warning is disabled because of a bug in VC6.
// It supresses warnings about too long debug symbols 
#pragma warning(disable: 4786)

#include "CGFaceTree.h"

#include "cgtexture.h"
#include "3dclasses.h"


#include "CGObject.h"
#include "DisallowCopy.h"
#include "PtrList.h"

#include "CGRoom.h"

#include "CGFaceTreeIterator.h"
#include "CGPortal.h"
#include "CGFaceID.h"
#include "Timer.h"
#include "CGPath.h"
#include "SkyMap.h"

#include <iostream>
// This warning is disabled because of a bug in VC6.
// It supresses warnings about too long debug symbols 
#pragma warning (disable: 4786)
#include <map>
#include <string>
#include "CGDynamicObject.h"


class PortalEngine;
class Camera;
class FaceRenderer;

// The class that binds all CG* classes together. One CGWorld for each cgworld file.
class CGWorld : private DisallowCopy
{
public:
	CGWorld(void);
	virtual ~CGWorld(void);

	// Initialize the world, this should be called always when GLUT is initialized!
	void init(void);

	// Save or load this world
	bool save(const char *pFilename);
	bool load(const char *pFilename);

	// Render one frame
	void render();

	// Create a new CGObject
	CGObject *newObject();

	// Create a new texture, returns its ID
	int newTexture(const char *pTextureFilename);
	
	// Draw a face by its face ID
	void drawByID(CGFaceID id) const;

	// Load all textures
	bool loadTextures();

	// Find a texture on filename
	int  findTexture(const char *pTextureFilename) const;

	// Activate a texture by ID
	void activateTexture(int id) const;

	// Get the face tree
	CGFaceTree * getFaceTree();
	const CGFaceTree * getFaceTree() const;

	// Create a new room
	int newRoom(void);

	// Get a room by ID
	CGRoom *getRoom(int id);

	// Create a new portal
	int newPortal(const Vertex3 points[CG_PORTAL_POINT_COUNT],
				  const int roomIDs[CG_PORTAL_ROOM_COUNT]);

	// Get a portal by ID
	CGPortal * getPortal(int id);

	// Find/new path
	CGPath * findPath(const char *pPathName);
	CGPath * newPath(const char *pPathName);

	// New/get dynamic object
	CGDynamicObject * newDynamicObject(const char *pName);
	CGDynamicObject * getDynamicObject(const char *pName);	

	// Get room or portal counts
	int	getRoomCount() const;
	int	getPortalCount() const;
	
	// Get the renderer used
	FaceRenderer	*getRenderer(void) const;
	
	// Get a face tree iterator
	CGFaceTreeIterator getIterator(NodeSelector *pSelector) const;
	
	// Get a triangle by face ID
	Triangle getTriangle(CGFaceID id) const;
	
	// Get the camera
	Camera * getCamera();

	// Get the timer
	Timer & getTimer();

private:
	PtrList<CGObject>		_objects;			// All CGObjects
	PtrList<CGTexture>		_textures;			// All CGTextures
	PtrList<CGRoom>			_rooms;				// All CGRooms
	PtrList<CGPortal>		_portals;			// All CGPortals
	PtrList<CGPath>			 _paths;			// All CGPaths
	PtrList<CGDynamicObject>	_dynamicObjects;	// All CGDynamicObjects
	std::map<std::string, int>  _dynamicObjectNameMap; // Name->CGDynamicObject index map
	
	CGFaceTree				_faceTree;			// Face tree
	Camera					*_pCamera;			// Camera
	PortalEngine			*_pPortalEngine;	// Portal engine
	Timer					_timer;				// Timer
	FaceRenderer			*_pRenderer;		// Renderer
	SkyMap					*_pSkyMap;			// Sky map
};

inline Triangle CGWorld::getTriangle(CGFaceID id) const
{
	const CGObject *pObj = _objects[id.objectIdx];
	return pObj->getTriangleAt(id.faceIdx);
}

inline void CGWorld::drawByID(CGFaceID id) const
{
	const CGObject *pObj = _objects[id.objectIdx];
	pObj->drawFace(this, id.faceIdx);
}

#endif
