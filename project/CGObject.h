/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#ifndef _CG_CGOBJECT_H_
#define _CG_CGOBJECT_H_

#include "AutoArr.h"
#include "DataFile.h"
#include "3dclasses.h"
#include "Face.h"
#include "Triangle.h"
#include "DisallowCopy.h"


const int CGOBJECT_VERTEX_SIZE  = 3;	  // in number of floats
const int CGOBJECT_NORMAL_SIZE  = 3;	  // in number of floats
const int CGOBJECT_TEXCOORD_SIZE = 2;
const int CGOBJECT_TEXCOORDSET_SIZE = CGOBJECT_TEXCOORD_SIZE * 3;
const int CGOBJECT_TRIANGLE_SIZE = CGOBJECT_VERTEX_SIZE * 3; // in number of floats, 3 vertices of 3 elements

class CGWorld;
class FaceRenderer;

// A 3D object in the cgworld file
class CGObject : private DisallowCopy
{
public:
	CGObject(void);
	virtual ~CGObject(void);

	// (Un)serialize
	void exchange(DataFile *pFile);

    void setTriangleCount(int count);
	void setFaceAt(int idx, const Face &face);

	// Returns a triangle at index idx
	Triangle getTriangleAt(int idx) const;
	int getTriangleCount() const;

	// Draws one face of the object
	void drawFace(const CGWorld *pWorld, int idx) const;

	// All sorts of set functions:
	void setHasLightmap(bool hasLightMap);

	void setAmbientColor(float color[4]);
	void setDiffuseColor(float color[4]);
	void setSpecularColor(float color[4]);
	void setShininess(float shininess);
	void setTransparent(bool transparent);

	void setTextureID(int id);
	void setLightmapTextureID(int id);

	// Sets up the object's material
	void setupMaterial(void) const;

	// Prepare before rendering
	virtual void prepareRender(FaceRenderer *pRenderer) const;

	// Clean up after rendering
	virtual void finishRender(FaceRenderer *pRenderer) const;

private:
	AutoArr<float> _vertexData;				// Vertex array (x,y,z float triples, 3 for each triangle)
	AutoArr<float> _normalData;				// Normal array (x,y,z float triples for each vertex)
	AutoArr<float> _texCoordData;			// Texture coordinate array (s,t pairs for each vertex)
	AutoArr<float> _lightTexCoordData;		// Lightmap coordinate array (s,t pairs for each vertex)
	
	bool		   _hasLightMap;			// True if the object has a lightmap
	int			   _texID;					// CGTexture id
	int			   _lightTexID;				// CGTexture lightmap id
	int			   _triangleCount;			// Number of triangles

	float		   _diffuseColor[4];		
	float		   _ambientColor[4];
	float		   _specularColor[4];
	float		   _shininess;				
	bool		   _transparent;			// True if the object is in any way transparent

	
};


inline int CGObject::getTriangleCount() const
{
	return _triangleCount; 
}

#endif
