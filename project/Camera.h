/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#ifndef _CG_CAMERA_H_
#define _CG_CAMERA_H_

#include "Vertex.h"
#include "Vector.h"
#include "DisallowCopy.h"

class CollisionTester;
class CGWorld;

// The radius in each dimension of the ellipsoid used for collision detection
const float CAMERA_RADIUS_X = 5.0, 
			CAMERA_RADIUS_Y = 10.0, 
			CAMERA_RADIUS_Z = 5.0;

// This determines the camera position relative
// to the center point of the collision ellipsoid.
const Vector3 CAMERA_SHIFT_VEC(0, 9, 0);

// Gravity acceleration
const float CAMERA_GRAVITY = 100.0f;				// in units/sec^2

// Minimum gravity speed
const float CAMERA_MIN_GRAVITY_SPEED = 30.0f;		// in units/sec 

// Maximum gravity speed
const float CAMERA_MAX_GRAVITY_SPEED = 5000.0f;		// in units/sec

const float MAX_PITCH =  88.0f,
	        MIN_PITCH = -88.0f;

const float CAMERA_NEAR = 1.0f, 
			CAMERA_FAR = 1000.0f, 
			CAMERA_FOV = 45.0f;
// The camera class
class Camera : private DisallowCopy
{
public:
	Camera(CGWorld *pWorld);
	virtual ~Camera(void);

	// Setup model-view matrix for the camera
	void	look(void);

	// Setup model-view matrix only for the direction of the camera
	void	lookInDirection(void);

	// Set position
	void	setPosition(const Vertex3 &pos);

	// Update the camera with a given walk and strafe distance
	// Collisions are tested
	void	update(float walk, float strafe);

	// Change yaw and pitch angles (angle is relative)
	void	yaw(float angle);
	void	pitch(float angle);

	// Get the position of the camera
	Vertex3 getPosition(void) const;
private:
	// Moves the camera, checking collisions
	void move(Vector3 velocity);

	Vertex3			_position;				// Current position
	float			_yaw;					// Yaw looking angle
	float			_pitch;					// Pitch looking angle
	CGWorld			*_pWorld;				// Reference to the world
	CollisionTester *_pCollisionTester;		// To test collisions
	float			_fallingSpeed;			// Current falling speed
	double			_lastUpdate;			// Last time the position was updated
};

#endif
