/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#ifndef _CG_CGPORTAL_H_
#define _CG_CGPORTAL_H_

#include "DisallowCopy.h"
#include "3dclasses.h"
#include "DataFile.h"

const int CG_PORTAL_POINT_COUNT = 4;
const int CG_PORTAL_ROOM_COUNT = 2;

class PlaneClipper;

// Represents a portal for the portal engine
class CGPortal : private DisallowCopy
{
public:

	CGPortal(void);
	
	// Initializes the portal with the given points and room IDs
	CGPortal(const Vertex3 points[CG_PORTAL_POINT_COUNT], 
			 const int roomIDs[CG_PORTAL_ROOM_COUNT]);
	virtual ~CGPortal(void);
	
	// Get a room ID from an index, or -1 if the index is invalid
	int getRoomID(int index) const
	{
		if (index < CG_PORTAL_ROOM_COUNT)
			return _roomIDs[index];
		return -1;
	}

	// Get the bounding box of the portal
	BoundingBox getBoundingBox() const { return _boundingBox; }

	// (Un)serialize the portal
	void exchange(DataFile *pFile);

	// Add the parts visible from cameraPos through the portal to a plane clipper
	void addToClipper(PlaneClipper *pClipper, const Vertex3 &cameraPos);

private:
	Vertex3	_points[CG_PORTAL_POINT_COUNT];		// Points of the portal
	int		_roomIDs[CG_PORTAL_ROOM_COUNT];		// Rooms the portal connects
	
	BoundingBox _boundingBox;			// Bounding box of the portal
	Vertex3		_center;				// Center point of the portal's points
	
};

#endif

