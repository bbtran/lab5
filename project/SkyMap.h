/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#ifndef _CG_SKYMAP_H_
#define _CG_SKYMAP_H_


#include "CGtexture.h"
#include <GL/glut.h>

typedef GLfloat point3[3];
typedef GLfloat point2[2];

/*         3 --- 2
  		  /     /|          y
		 0 --- 1 |          |
 		 | 7   | 6          . -- x
		 |     |/          /
		 4 --- 5          z
*/
const point3 SKYMAP_CORNERS[8] =
	{
		{-10, +10, +10},		// 0
		{+10, +10, +10},		// 1
		{+10, +10, -10},		// 2
		{-10, +10, -10},		// 3

		{-10, -10, +10},		// 4
		{+10, -10, +10},		// 5
		{+10, -10, -10},		// 6
		{-10, -10, -10}		// 7
	};

const point2 SKYMAP_TEX_COORDS[4] =
	{	
		{1.0/128.0, 1.0/128.0}, {127.0/128.0, 1.0/128.0}, {127.0/128.0, 127.0/128.0}, {1.0/128.0, 127.0/128.0}
	};

// Vertex indices of the cube's faces
const int SKYMAP_FACE_CORNER_IDX[6][4] =
	{
		{0, 3, 2, 1},	// top
		{1, 5, 4, 0},	// back
		{2, 6, 5, 1},	// right 
		{3, 7, 6, 2},	// front
		{0, 4, 7, 3},	// left 
		{7, 4, 5, 6}	// bottom
	};

const int SKYMAP_TEX_CORNER_IDX[6][4] =
	{
		{1, 2, 3, 0},	// top
		{3, 0, 1, 2},	// back
		{3, 0, 1, 2},	// right 
		{3, 0, 1, 2},	// front
		{3, 0, 1, 2},	// left 
		{1, 2, 3, 0},	// bottom
	};

const char * const SKYMAP_FILENAMES[6]	= 
	{	
		"../textures/sky_top.rgb",
		"../textures/sky_back.rgb",		
		"../textures/sky_left.rgb",	
		"../textures/sky_front.rgb",
		"../textures/sky_right.rgb",	
 		"../textures/sky_bottom.rgb",
	};

// Skymap class
class SkyMap
{
public:
	SkyMap(void);
	virtual ~SkyMap(void);
	void draw(void);
private:
	CGTexture *_textures[6];
	

};

#endif
