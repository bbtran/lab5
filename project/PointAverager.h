/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#ifndef _CG_POINTAVERAGER_H_
#define _CG_POINTAVERAGER_H_

#include "Vertex.h"

/* This class can be used to determine the average of a 
   set of points (vertices). */
class PointAverager
{
public:

	// Initializes the averager
	PointAverager(void)
	{
		reset();
	}

	~PointAverager(void)
	{
	}

	// Resets the average to 0
	void reset(void)
	{
		_x = _y = _z = _count = 0;
	}

	// Adds a point to the average
	void add(Vertex3 p)
	{
		_x += p.x();
		_y += p.y();
		_z += p.z();
		_count += 1.0;
	}

	// Returns the average point of all points added up till now
	Vertex3 getAverage() const
	{
		return Vertex3(_x/_count, _y/_count, _z/_count);
	}

private:
	float _x, _y, _z;	// Sums of each dimension
	float _count;		// Number of points added
};


#endif

