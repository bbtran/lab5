/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#include "CGPortal.h"
#include "PointAverager.h"
#include "Vertex.h"
#include "PlaneClipper.h"

CGPortal::CGPortal(void)
{
}

CGPortal::CGPortal(const Vertex3 points[CG_PORTAL_POINT_COUNT], 
				   const int roomIDs[CG_PORTAL_ROOM_COUNT])
{
	// Copy points and room IDs
	memcpy(_points, points, sizeof(_points));
	memcpy(_roomIDs, roomIDs, sizeof(_roomIDs));
	
	// Average all points to get the center and create
	// a bounding box at the same time
	PointAverager avg;
	for (int i=0;i<CG_PORTAL_POINT_COUNT;i++)
	{
		Vertex3 point = _points[i];
		avg.add(point);
		if (i==0)
			_boundingBox = BoundingBox(point, point);
		else
			_boundingBox.include(point);
	}
	_center = avg.getAverage();
}



CGPortal::~CGPortal(void)
{
}

void CGPortal::exchange(DataFile *pFile)
{
	for (int i=0;i<CG_PORTAL_POINT_COUNT;i++)
	{
        _points[i].exchange(pFile);
	}
	pFile->exchange(_roomIDs, CG_PORTAL_ROOM_COUNT);
	_boundingBox.exchange(pFile);
	_center.exchange(pFile);
}

void CGPortal::addToClipper(PlaneClipper *pClipper, const Vertex3 &cameraPos)
{
	for (int i=0;i<CG_PORTAL_POINT_COUNT;i++)
	{
		// Create plane for each edge of the portal. Each plane consists of 
		// two portal points that are next to each other and the camera position
		// as the third point.
		Plane p(_points[i], _points[(i+1) % CG_PORTAL_POINT_COUNT], cameraPos);
		
		// The planes should always front the center position
		if (p.side(_center) < 0)
			p.flip();

		// Add the plane to the clipper
		pClipper->addPlane(p);
	}
}

