/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#ifndef _CG_FACERENDERER_H_
#define _CG_FACERENDERER_H_

#define WIN32_LEAN_AND_MEAN

#include "CGWorld.h"
#include <GL/glut.h>
#include <windows.h>
#include <iostream>
#include "CGObject.h"
#include "PointAverager.h"
#include <algorithm>
#include "util.h"
#include "Statistics.h"

// Typedef and defines for the multitexturing extension
typedef void (APIENTRY * PFNGLACTIVETEXTUREARBPROC) (GLenum texture);
typedef void (APIENTRY * PFNGLMULTITEXCOORD2FVARBPROC) (GLenum target, const GLfloat *v);
#define GL_TEXTURE0_ARB                   0x84C0
#define GL_TEXTURE1_ARB                   0x84C1

// Face renderer
class FaceRenderer
{
public:
	// Data for one face
	struct FaceData
	{
		const CGObject *pObject;
		const float *pTexCoord;
		const float *pNormals;
		const float *pVertices;
		const float *pLightTexCoord;
		bool	transparent;
	};

	FaceRenderer(CGWorld *pWorld)
	{
		_pWorld = pWorld;
		_multiTexSupported = loadExtensions();
		_pCurObject = NULL;
		_curLightTexID = -1;
		_curTexID = -1;
	}

	void startFrame()
	{
		setTexture(-1);
		setLightTexture(-1);
		_pCurObject = NULL;
	}

	void endFrame()
	{
		switchToObject(NULL);
		setTexture(-1);
		setLightTexture(-1);
	}
	
	
	// Queues a transparent face for rendering later
	void queueTransparentFace(const FaceData &data)
	{
		_queuedFaces.push_back(data);
	}

	// Draws a face. If queueTransparent is true, a transparent face is 
	// not drawn yet but queued. Otherwise the face is always drawn.
	void draw(const FaceData &data, bool queueTransparent = true)
	{
		const CGObject *pObject = data.pObject;
		
		const float *pTexCoord = data.pTexCoord;
		const float *pNormals = data.pNormals;
		const float *pVertices = data.pVertices;
		const float *pLightTexCoord = data.pLightTexCoord;
		
		switchToObject(pObject);

		// Transparent face?
		if (data.transparent)
		{
			// Queue if asked for
			if (queueTransparent)
			{
				queueTransparentFace(data);
				pObject->finishRender(this);
				return;
			}
			else
			{
				// Enable blending
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			}
		}
		else
		{
			// No blending for opaque faces
			glDisable(GL_BLEND);
		}
		
		// Use multitexturing if it is supported and there is a lightmap
		bool useMulti = (_multiTexSupported && _curLightTexID >= 0);

		// Draw triangles
		glBegin(GL_TRIANGLES);
			for (int i=0;i<3;i++)
			{
				glTexCoord2fv(pTexCoord);
				glNormal3fv(pNormals);
				if (useMulti)
				{
					// Lightmap texture coordinates
					_pglMultiTexCoord2fv(GL_TEXTURE1_ARB, pLightTexCoord);
					pLightTexCoord += 2;
				}
				glVertex3fv(pVertices);
				
				// Move to next vertex values
				pVertices += 3;
				pNormals  += 3;
				pTexCoord += 2;
			}
		glEnd();
	}

public:
	// Switch renderer data to a new object
	void switchToObject(const CGObject *pObj)
	{
		if (_pCurObject == pObj)
			return; // nothing to do

		if (_pCurObject)
		{
			// Clean old object 
			_pCurObject->finishRender(this);
			_pCurObject = NULL;
		}

		// Set new object:
		_pCurObject = pObj;
	
		if (_pCurObject)
			_pCurObject->prepareRender(this);
		
		
		Statistics::instance().objectSwitches++;
	}

	// Sets the current texture to the given texure ID
	void setTexture(int texID)
	{
		if (_curTexID == texID)
			return;	// nothing to do

		_curTexID = texID;
		if (texID < 0)
		{
			glDisable(GL_TEXTURE_2D);
		}
		else
		{
			Statistics::instance().textureSwitches++;
			glEnable(GL_TEXTURE_2D);
			_pWorld->activateTexture(texID); 
		}
	}

	
	// Sets the lightmap texture to the given texure ID
	void setLightTexture(int texID)
	{
		if (_curLightTexID == texID)
			return;	// nothing to do

		if (_multiTexSupported)
		{
			
			_curLightTexID = texID;
			_pglActiveTexture(GL_TEXTURE1_ARB);
			if (texID < 0)
			{
				// Disable, invalid ID
				glDisable(GL_TEXTURE_2D);
			}
			else
			{
				// Enable and set mode to modulation
				glEnable(GL_TEXTURE_2D);
				glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
				_pWorld->activateTexture(texID);
				Statistics::instance().textureSwitches++;
			}
			_pglActiveTexture(GL_TEXTURE0_ARB);
		}
		else
		{
			_curLightTexID = -1;
		}
	}

	// Draw the transparent faces that were queued
	void drawQueued(void)
	{

		// Actually, transparent faces should be ordered back to front
		// However this is not really important in this scene so I left it out
		// and just disabled the depth buffer.

		// Disable depth buffer
		glDepthMask(GL_FALSE);
		
		// Draw the transparent faces
		std::vector<FaceData>::iterator it;
		for (it=_queuedFaces.begin();it!=_queuedFaces.end();it++)
		{
			draw(*it, false);
		}
		
		// Enable depth buffer again
		glDepthMask(GL_TRUE);

		// Clear queue
		_queuedFaces.clear();
	}

	// Load extensions if present
	bool loadExtensions()
	{
		// Extension present?
		const char *pExtensionString = reinterpret_cast<const char*>(glGetString(GL_EXTENSIONS));
		if (!pExtensionString)
			return false;

		if (!strstr(pExtensionString, "GL_ARB_multitexture"))
			return false;

		// Load two functions 

		_pglActiveTexture = reinterpret_cast<PFNGLACTIVETEXTUREARBPROC>(
									wglGetProcAddress("glActiveTextureARB"));
		if (!_pglActiveTexture)
			return false;

		_pglMultiTexCoord2fv =  reinterpret_cast<PFNGLMULTITEXCOORD2FVARBPROC>(
								wglGetProcAddress("glMultiTexCoord2fvARB"));
		
		if (!_pglMultiTexCoord2fv)
			return false;

		return true;
	}
	// Extension function pointers
	PFNGLACTIVETEXTUREARBPROC		_pglActiveTexture;
	PFNGLMULTITEXCOORD2FVARBPROC	_pglMultiTexCoord2fv;
	bool	_multiTexSupported;
	
	CGWorld *_pWorld;
	
	// Queued transparent faces
	std::vector<FaceData>	_queuedFaces;

	// Current texture IDs and object
	int		_curTexID;
	int		_curLightTexID;
	const CGObject *_pCurObject;
	
};

#endif

