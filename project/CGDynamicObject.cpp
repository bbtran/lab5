/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#pragma warning (disable: 4786)
#include "CGDynamicObject.h"
#include "NodeSelector.h"
#include <GL/glut.h>
#include "CGWorld.h"
#include "Statistics.h"
#include "util.h"


CGDynamicObject::CGDynamicObject(void): _position(Vertex3(0)), _yaw(0.0), _pitch(0.0)
{
	_texShift[0] = 0.0;
	_texShift[1] = 0.0;

}

CGDynamicObject::~CGDynamicObject(void)
{
}

void CGDynamicObject::draw(CGWorld *pWorld, const NodeSelector *pNodeSel)
{
	/* First calculate a bounding box of the object in the current position. 
	   This bounding box includes *all* possible rotations of the object */
	
	// Get the maximum radius of the object:
	float rx = _box.dx(), 
		  ry = _box.dy(),
		  rz = _box.dz();
	float maxRadius = sqrt(rx * rx + ry * ry + rz * rz);

	// Create a bounding box from the position and the radius in all directions:
	Vector3 radiusVec(maxRadius);
	BoundingBox realBox(_position - radiusVec, _position + radiusVec);
	
	// If this box is not selected do not draw it
	if (!pNodeSel->includes(realBox))
		return;
	
	// Draw all faces
	int faceCount = getTriangleCount();
	for (int i=0;i<faceCount;i++)
	{
		drawFace(pWorld, i);
	}

	// Update statistics
	Statistics::instance().dynamicTrianglesRendered += faceCount;
}

void CGDynamicObject::exchange(DataFile *pFile)
{
	CGObject::exchange(pFile);
	pFile->exchange(&_name, CGDYNAMICOBJECT_MAX_NAME_LEN);
	_position.exchange(pFile);
	pFile->exchange(&_yaw);
	pFile->exchange(&_pitch);
	pFile->exchange(&_roll);
	_box.exchange(pFile);
}

void CGDynamicObject::setTextureShift(float s, float t)
{
	_texShift[0] = s;
	_texShift[1] = t;
}

void CGDynamicObject::prepareRender(FaceRenderer *pRenderer) const
{
	CGObject::prepareRender(pRenderer);

	// Save transformation matrix
	glPushMatrix();

	// Move object
	glTranslatef(_position.x(), _position.y(), _position.z());

	// Rotate object
	glRotatef(_yaw,   0.0, 1.0, 0.0); 
	glRotatef(_pitch, 1.0, 0.0, 0.0);
	glRotatef(_roll,  0.0, 0.0, 1.0);

	// Apply texture shift if necessary
	float ts_s =  _texShift[0];
	float ts_t =  _texShift[1];
	if (ts_s != 0.0 || ts_t != 0.0)
	{
		glMatrixMode(GL_TEXTURE);
		glTranslatef(ts_s, ts_t, 0.0);
		glMatrixMode(GL_MODELVIEW);
	}

}

void CGDynamicObject::setDirection(const Vector3 &direction)
{
	_yaw = rad2deg(atan2(-direction.z(), direction.x())) - 90.0;

	_pitch = rad2deg(asin(direction.y()));
	
	// Safety check for pitch, a pitch <= 90 or >=90 gives gimbal lock problems
	if (_pitch <= -90.0)
		_pitch = -89.999;
	else if (_pitch >= 90.0)
		_pitch = 89.999; 
	
	_roll = 0.0f;
		
}

void CGDynamicObject::finishRender(FaceRenderer *pRenderer) const
{
	// Restore transformation 
	glPopMatrix();
	
	// Restore texture shift if necessary
	float ts_s =  _texShift[0];
	float ts_t =  _texShift[1];
	if (ts_s != 0.0 || ts_t != 0.0)
	{
		glMatrixMode(GL_TEXTURE);
		glLoadIdentity();
		glMatrixMode(GL_MODELVIEW);
	}
}