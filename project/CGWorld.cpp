/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/


#include "cgworld.h"
#include "DataFile.h"
#include "cgtexture.h"
#include "CGPortal.h"
#include "CGFaceTree.h"
#include "NodeSelector.h"
#include "Light.h"
#include "Statistics.h"
#include "PortalEngine.h"
#include "Frustum.h"
#include "FaceRenderer.h"
#include <iostream>

using namespace std;

// Header of the cgworld file
char g_cgWorldHeader[] = "CGWORLD1";

Light g_light(50, 200, 50, 0);

CGWorld::CGWorld(void)
{
	_pCamera = new Camera(this);
	_pPortalEngine = new PortalEngine(this);
	_pRenderer = NULL;
}

CGWorld::~CGWorld(void)
{
	delete _pCamera;
	delete _pPortalEngine;
	delete _pRenderer;
	delete _pSkyMap;
}

void CGWorld::init(void)
{
	_pRenderer = new FaceRenderer(this);
	_pSkyMap = new SkyMap();
}

bool CGWorld::save(const char *pFilename)
{
	try
	{
		// Setup datafile
		DataFile cgw(pFilename, true);

		// Write file header
		cgw.exchange(g_cgWorldHeader, sizeof(g_cgWorldHeader)-1);

		// Save all child data
		_objects.exchange(&cgw);
		_textures.exchange(&cgw);
		_rooms.exchange(&cgw);
		_portals.exchange(&cgw);

		_paths.exchange(&cgw);
		_dynamicObjects.exchange(&cgw);
			
		_faceTree.exchange(&cgw);

	}
	catch(DataFile::Error e)
	{
		cerr << "DataFile error: " << e.what() << endl;
		return false;
	}
	return true;
}

bool CGWorld::load(const char *pFilename)
{
	try
	{
		// Setup datafile
		DataFile cgw(pFilename, false);

		// Check header
		char header[sizeof(g_cgWorldHeader)];
		header[sizeof(g_cgWorldHeader)-1] = 0;
		cgw.exchange(header, sizeof(g_cgWorldHeader)-1);
		if (strcmp(g_cgWorldHeader, header)!=0)
		{
			// Invalid header
			return false;
		}

		// Load all objects
		_objects.exchange(&cgw);
		_textures.exchange(&cgw);
		_rooms.exchange(&cgw);
		_portals.exchange(&cgw);
		_paths.exchange(&cgw);
		_dynamicObjects.exchange(&cgw);
		_faceTree.exchange(&cgw);
		
		if (!loadTextures())
			return false;
		
		// Setup the dynamic object name map
		for (int i=0;i<_dynamicObjects.size();i++)
		{
			_dynamicObjectNameMap[_dynamicObjects[i]->getName()] = i;
		}
	}
	catch(DataFile::Error e)
	{
		cerr << "DataFile error: " << e.what() << endl;
		return false;
	}
	return true;

}
CGObject * CGWorld::newObject()
{
	CGObject *pObj = new CGObject();
	_objects.add(pObj);
	return pObj;
}

CGDynamicObject * CGWorld::newDynamicObject(const char *pName)
{
	// Create a new object and add an entry in the name map
	CGDynamicObject *pObj = new CGDynamicObject();
	pObj->setName(pName);
	int idx = _dynamicObjects.size();
	_dynamicObjects.add(pObj);
	_dynamicObjectNameMap[std::string(pName)] = idx;
	return pObj;
}

CGDynamicObject * CGWorld::getDynamicObject(const char *pName)
{
	// Lookup the name
	std::map<std::string, int>::iterator it;
	it = _dynamicObjectNameMap.find(std::string(pName));
	if (it==_dynamicObjectNameMap.end())
		return NULL;

	// Return object if found
	return _dynamicObjects[it->second];
}

int CGWorld::newTexture(const char *pTextureFilename)
{
	int id = _textures.size();
	CGTexture *pTex = new CGTexture(pTextureFilename);
	_textures.add(pTex);
	return id;
}

int CGWorld::newPortal(const Vertex3 points[CG_PORTAL_POINT_COUNT], const int roomIDs[CG_PORTAL_ROOM_COUNT])
{
	int id = _portals.size();
	CGPortal *pPortal = new CGPortal(points, roomIDs);
	_portals.add(pPortal);
	return id;
}

CGPortal * CGWorld::getPortal(int id)
{
	if (id < _portals.size())
		return _portals[id];
	return NULL;
}

bool CGWorld::loadTextures()
{
	for (int i=0;i<_textures.size();i++)
	{
		if (!_textures[i]->loadTextureFile())
			return false;
	}
	return true;
}

int CGWorld::findTexture(const char *pTextureFilename) const
{
	for (int i=0;i<_textures.size();i++)
	{
		if (_textures[i]->getFilename() == pTextureFilename)
			return i;
	}
	return -1;
}

void CGWorld::activateTexture(int id) const
{
	if (id < 0 || id >= _textures.size())
		return;

	_textures[id]->activate();
}

CGFaceTree * CGWorld::getFaceTree()
{
	return &_faceTree;
}

const CGFaceTree * CGWorld::getFaceTree() const
{
	return &_faceTree;
}

int CGWorld::newRoom()
{
	int id = _rooms.size();
	CGRoom *pObj = new CGRoom();
	_rooms.add(pObj);
	return id;
}

CGRoom * CGWorld::getRoom(int id)
{
	if (id < _rooms.size())
		return _rooms[id];
	return NULL;
}

void CGWorld::render()
{
	glEnable(GL_BLEND);
		
	// --- Draw the sky ---
	_pCamera->lookInDirection();
	// Move the clouds a bit:
	glRotatef(_timer.getTime(), 1.0, 0.0, 0.0);
	_pSkyMap->draw();

	_pCamera->look();
	_pRenderer->startFrame();
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_BACK, GL_AMBIENT_AND_DIFFUSE);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	

	// Setup a simple light
	g_light.setupLight(0);

	// Build a portal node selector
	Frustum frustum;
	frustum.calculateFrustum();
	PortalNodeSelector sel = _pPortalEngine->getSelector(frustum);

	// Draw static faces
	Statistics &stats = Statistics::instance();
	int count = 0;
	CGFaceTreeIterator it = getIterator(&sel); 
	while (it.next())
	{
		drawByID(it.currentFace());
		count++;
	}
	stats.trianglesRendered = count;

	// Draw dynamic faces
	int dynamicObjectCount = _dynamicObjects.size();
	for (int i=0;i<dynamicObjectCount;i++)
	{
		_dynamicObjects[i]->draw(this, &sel);
	}
	_pRenderer->drawQueued();
	_pRenderer->endFrame();
}

CGFaceTreeIterator CGWorld::getIterator(NodeSelector *pSelector) const 
{
	return CGFaceTreeIterator(this, pSelector);
}

Camera * CGWorld::getCamera()
{
	return _pCamera;
}

Timer & CGWorld::getTimer()
{
	return _timer;
}


int	CGWorld::getRoomCount(void) const
{ 
	return _rooms.size(); 
}

int	CGWorld::getPortalCount(void) const
{
	return _portals.size(); 
}

FaceRenderer * CGWorld::getRenderer(void) const
{
	return _pRenderer;
}

CGPath * CGWorld::findPath(const char *pPathName)
{
	for (int i=0;i<_paths.size();i++)
	{
		if (_paths[i]->getName() == pPathName)
			return _paths[i];
	}
	return NULL;
}

CGPath * CGWorld::newPath(const char *pPathName)
{
	CGPath *pPath = new CGPath();
	pPath->setName(pPathName);
	_paths.add(pPath);
	return pPath;
}