/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#ifndef _CG_CGPATH_H_
#define _CG_CGPATH_H_


#include "DisallowCopy.h"
#include "DataFile.h"
#include <string>
#include "AutoArr.h"
#include "Vertex.h"
#include "Vector.h"
#include <cmath>
#include <iostream>
using namespace std;
const int CG_PATH_MAX_NAME_LEN = 100;

// A path in 3D
class CGPath : private DisallowCopy
{
public:
	CGPath(void);
	virtual ~CGPath(void);
	void exchange(DataFile *pFile);

	// Set new points
	void setPoints(const Vertex3 *pVertices, int count);
	
	// Set/get name for the path
	void setName(std::string name);
	std::string getName() const;

	// Get a point or direction at a given phase. The phase ranges from 0 to 1, and 
	// indicates which spot on the path the point or direction should be taken.
	Vertex3 getPointAt(float phase) const;
	Vector3 getDirectionAt(float phase) const;

private:


	static float bezierInterpolate(float y1, float y2, float y3, float y4, float m)
	{
		float r = 1.0f - m;

		float a = y2;
		float b = y2 + (y2 - y1) / 3.0f;
		float c = y3 + (y3 - y4) / 3.0f;
		float d = y3;

		return a * r * r * r + b * 3 * m * r * r + c * 3 * m * m * r + d * m * m * m;
	}
	

	static Vertex3 bezierInterpolate(Vertex3 v0, Vertex3 v1, Vertex3 v2, Vertex3 v3, float m)
	{
		float x = bezierInterpolate(v0.x(), v1.x(), v2.x(), v3.x(), m);
		float y = bezierInterpolate(v0.y(), v1.y(), v2.y(), v3.y(), m);
		float z = bezierInterpolate(v0.z(), v1.z(), v2.z(), v3.z(), m);
		return Vertex3(x, y, z);
	}

	static Vector3 bezierInterpolate(Vector3 v0, Vector3 v1, Vector3 v2, Vector3 v3, float m)
	{
		float x = bezierInterpolate(v0.x(), v1.x(), v2.x(), v3.x(), m);
		float y = bezierInterpolate(v0.y(), v1.y(), v2.y(), v3.y(), m);
		float z = bezierInterpolate(v0.z(), v1.z(), v2.z(), v3.z(), m);
		return Vector3(x, y, z);
	}

	AutoArr<Vertex3>	_points;		// The points of the path
	std::string			_name;			// Name of the path
};

#endif

