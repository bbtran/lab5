/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#pragma warning (disable: 4786)

#include "cgobject.h"
#include <cstdlib>
#include "CGWorld.h"
#include <GL/gl.h>
#include "FaceRenderer.h"

using namespace std;

CGObject::CGObject(void): _hasLightMap(false),
	_texID(-1), _lightTexID(-1), _transparent(false)
{
}

CGObject::~CGObject(void)
{
}

void CGObject::exchange(DataFile *pFile)
{
	_vertexData.exchange(pFile);
	_normalData.exchange(pFile);
	_texCoordData.exchange(pFile);
	pFile->exchange(&_hasLightMap);
	if (_hasLightMap)
	{
		_lightTexCoordData.exchange(pFile);
	}
	pFile->exchange(&_texID);
	pFile->exchange(&_lightTexID);
	if (!pFile->writing())
	{
		_triangleCount = _vertexData.size() / CGOBJECT_TRIANGLE_SIZE;
	}
	pFile->exchange(_diffuseColor, 4);
	pFile->exchange(_ambientColor, 4);
	pFile->exchange(_specularColor, 4);
	pFile->exchange(&_shininess);
	pFile->exchange(&_transparent);

}

void CGObject::setTriangleCount(int count)
{
	_vertexData.alloc(count * CGOBJECT_TRIANGLE_SIZE);
	_normalData.alloc(count * CGOBJECT_TRIANGLE_SIZE);
	_texCoordData.alloc(count * CGOBJECT_TEXCOORDSET_SIZE);
	_triangleCount = count;
}

void CGObject::setAmbientColor(float color[4])
{	
	memcpy(_ambientColor, color, sizeof(_ambientColor));
}

void CGObject::setDiffuseColor(float color[4])
{	
	memcpy(_diffuseColor, color, sizeof(_diffuseColor));
}

void CGObject::setSpecularColor(float color[4])
{	
	memcpy(_specularColor, color, sizeof(_specularColor));
}

void CGObject::setShininess(float shininess)
{	
	_shininess = shininess;
}

void CGObject::setTransparent(bool transparent)
{
	_transparent = transparent;
}

void CGObject::setTextureID(int id)
{
	_texID = id;
}

void CGObject::setLightmapTextureID(int id)
{
	_lightTexID = id;
}

void CGObject::setFaceAt(int idx, const Face &face)
{
	int j;
	// Calculate pointers to triangle vertices and normals
	float *pTriangleBase = _vertexData.get() + idx * CGOBJECT_TRIANGLE_SIZE;
	float *pNormalBase   = _normalData.get() + idx * CGOBJECT_TRIANGLE_SIZE;

	// Copy vertices and normals
	for (j=0;j<3;j++)
	{
        memcpy(pTriangleBase + j * CGOBJECT_VERTEX_SIZE, face.points[j].ref(),  sizeof(float) * CGOBJECT_VERTEX_SIZE);
		memcpy(pNormalBase   + j * CGOBJECT_NORMAL_SIZE, face.normals[j].ref(), sizeof(float) * CGOBJECT_NORMAL_SIZE);
	}

	// Copy texture coordinates
	float *pTexCoordBase = _texCoordData.get() + idx * CGOBJECT_TEXCOORDSET_SIZE;
	memcpy(pTexCoordBase, face.tex, sizeof(float) * CGOBJECT_TEXCOORDSET_SIZE);

	// Copy lightmap coordinates, if available
	if (_hasLightMap)
	{
		float *pLightTexCoordBase = _lightTexCoordData.get() + idx * CGOBJECT_TEXCOORDSET_SIZE;
		memcpy(pLightTexCoordBase, face.lightTex, sizeof(float) * CGOBJECT_TEXCOORDSET_SIZE);
	}
}

Triangle CGObject::getTriangleAt(int idx) const
{
	const float *pTriangleBase = _vertexData.get() + idx * CGOBJECT_TRIANGLE_SIZE;
	
	Vertex3 v1(pTriangleBase[0], pTriangleBase[1], pTriangleBase[2]);
	Vertex3 v2(pTriangleBase[3], pTriangleBase[4], pTriangleBase[5]);
	Vertex3 v3(pTriangleBase[6], pTriangleBase[7], pTriangleBase[8]);
	
	return Triangle(v1, v2, v3);
}


void CGObject::setHasLightmap(bool hasLightMap)
{
	_hasLightMap = hasLightMap;
	_lightTexCoordData.alloc(getTriangleCount() * CGOBJECT_TEXCOORDSET_SIZE);
}

void CGObject::setupMaterial(void) const
{
	if (_hasLightMap)
	{
		float color[4];
		color[0] = 1.0;
		color[1] = 1.0;
		color[2] = 1.0;
		color[3] = 1.0;

		// Setup material properties
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, color);
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, color);
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, color);
		glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, _shininess); 
	}
	else
	{
		// Setup material properties
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, _ambientColor);
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, _diffuseColor);
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, _specularColor);
		glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, _shininess); 
	}
}

void CGObject::drawFace(const CGWorld *pWorld, int idx) const
{
	// Setup a FaceData structure with all the pointers
	FaceRenderer::FaceData data;
	
	data.pObject = this;
	data.pVertices  = _vertexData.get() + idx * CGOBJECT_TRIANGLE_SIZE;
	data.pNormals   = _normalData.get() + idx * CGOBJECT_TRIANGLE_SIZE;
	data.pTexCoord = _texCoordData.get() + idx * CGOBJECT_TEXCOORDSET_SIZE;
	data.pLightTexCoord = NULL; 
	data.transparent = _transparent;

	// Add lightmap coordinates if available
	if (_hasLightMap)
	{
		data.pLightTexCoord = _lightTexCoordData.get() + idx * CGOBJECT_TEXCOORDSET_SIZE;
	}

	// Render the face
	pWorld->getRenderer()->draw(data);
}


void CGObject::prepareRender(FaceRenderer *pRenderer) const
{
	glEnable(GL_LIGHTING);
	// Setup material and textures
	setupMaterial();
	pRenderer->setTexture(_texID);
	pRenderer->setLightTexture(_lightTexID);
}

void CGObject::finishRender(FaceRenderer *pRenderer) const
{
	// Nothing to do here
}