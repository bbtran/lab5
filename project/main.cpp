/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#include <GL/glut.h>
#include <util/glutil.h>
#include <stdio.h>
#include <iostream>
#include <string>

#include "3dclasses.h"
#include "CGWorld.h"
#include "Camera.h"
#include "InputManager.h"
#include "../external/3DSLoader/Model_3DS.h"
#include "Statistics.h"
#include "Seahorse.h"

using namespace std;

CGWorld g_world;

// 3DS Models
Model_3DS grass;
CGPath *g_pSeahorsePath;
Seahorse seahorse;

int aquariumSceneDL;
InputManager	g_inputManager(&g_world);

const Vertex3 CAMERA_POS(450, 0, 390);
const float   CAMERA_YAW = -90.0, CAMERA_PITCH = 0.0;

const char WORLD_FILE[] = "../models/zoo.cgworld";
char GRASS_FILE[] = "../models/grass.3ds";

#ifdef _DEBUG
const char WINDOW_TITLE[] = "Lab 5 - Aquarium by Binh Tran and Winston Kyu WARNING: Running in debug mode harms performance! ";
#else
const char WINDOW_TITLE[] = "Lab 5 - Aquarium by Binh Tran and Winston Kyu";
#endif


// Draws a string at x,y. String may use \n for newlines	
void outputText(int x, int y, const char *string)
{
	int len, i;
	len = (int) strlen(string);

	int lines = 1;
	for (i = 0; i < len; i++)
	{
		if (string[i]=='\n') 
			lines++;
	}

	int line = 0;
	glRasterPos2f(x, y - line * 16);
	for (i = 0; i < len; i++)
	{
		char c = string[i];
		if (c=='\n')
		{
			line++;
			glRasterPos2f(x, y - line * 16);
		}
		else
		{
			glutBitmapCharacter(GLUT_BITMAP_9_BY_15, c);
		}
	}
}

void showStats()
{
	// Disable some stuff
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_TEXTURE_2D);
	
	// Setup ortho projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT),  -10, 10);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Write statistics
	glColor3f(1,0,0);
	string str = Statistics::instance().getFrameString();
	outputText(4,glutGet(GLUT_WINDOW_HEIGHT) - 20, str.c_str());
}

void initAquariumSceneDL() {
	aquariumSceneDL = glGenLists(1);
	glNewList(aquariumSceneDL, GL_COMPILE);
	
	glPushMatrix();
	glTranslatef(520.0f, -35.0f, 390.0f);
	
	// Draw the great big patch of grass
	glPushMatrix();
	glTranslatef(10.0f, 0.0f, -65.0f);
	glScalef(1.0f, 3.0f, 1.0f);
	grass.Draw();
	glTranslatef(0.0f, 0.0f, 25.0f);
	grass.Draw();
	glTranslatef(10.0f, 0.0f, 20.0f);
	grass.Draw();
	glTranslatef(10.0f, 0.0f, 20.0f);
	grass.Draw();
	glTranslatef(5.0f, 0.0f, 20.0f);
	grass.Draw();
	glTranslatef(0.0f, 0.0f, 20.0f);
	grass.Draw();
	glPopMatrix();
	
	glPopMatrix();

	glEndList();
}

void display(void)
{
	glClearColor(0.0, 0.0, 0.4, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// Update stats
	Statistics::instance().onNewFrame(g_world.getTimer().getTime());

	// Update input manager
	g_inputManager.onFrame();

	// Render the world
	g_world.render();

	glCallList(aquariumSceneDL);
	seahorse.draw();

	// Show stats
	if (g_inputManager.shouldShowStats())
		showStats();

	// Finish
	Statistics::instance().resetFrameStats();
	glutSwapBuffers();
}

void animateAquarium(float time)
{
	CGDynamicObject *pObj = NULL;
	CGPath *pPath = NULL;

	// Animate bubbles
	pObj = g_world.getDynamicObject("bubbles");
	if (pObj)
		pObj->setTextureShift(0, -fmod(time/2.0f, 1.0f));
	pObj = g_world.getDynamicObject("bubbles2");
	if (pObj)
		pObj->setTextureShift(0, -fmod(time/2.1f + 0.3f, 1.0f));
	// Animate water
	pObj = g_world.getDynamicObject("water");
	if (pObj)
		pObj->setTextureShift(sin(time/2.0f) / 10.0f, cos(time * 0.65f)/ 10.0f);

	pObj = g_world.getDynamicObject("water2");
	if (pObj)
		pObj->setTextureShift(sin(time/2.5f) / 10.0f, cos(time * 0.50f)/ 10.0f);

	// Animate water lights
	pObj = g_world.getDynamicObject("waterlight1");
	if (pObj)
		pObj->setTextureShift(0, fmod(time/10.0f, 1.0f));

	pObj = g_world.getDynamicObject("waterlight2");
	if (pObj)
		pObj->setTextureShift(0, fmod(time/10.0f, 1.0f));

	// Dirt in the water
	pObj = g_world.getDynamicObject("waterDirt");
	if (pObj)
		pObj->setTextureShift(fmod(time/10.0f, 1.0f),sin(time/4.0f) * 0.5f);
	
	pObj = g_world.getDynamicObject("waterDirt2");
	if (pObj)
		pObj->setTextureShift(fmod(time/11.0f, 1.0f),cos(time/7.0f) * 0.5f);

	// Animate fishes
	pObj = g_world.getDynamicObject("koraalvlinder2");
	pPath = g_world.findPath("fish1");
	if (pObj && pPath)
	{
		pObj->setPosition(pPath->getPointAt(time / 15.0f));
		pObj->setDirection(pPath->getDirectionAt(time / 15.0f));
	}
	pObj = g_world.getDynamicObject("nemo2");
	pPath = g_world.findPath("fish2");
	if (pObj && pPath)
	{
		pObj->setPosition(pPath->getPointAt(time / 20.0f));
		pObj->setDirection(pPath->getDirectionAt(time / 20.0f));
	}
	pObj = g_world.getDynamicObject("nemo3");
	pPath = g_world.findPath("fish3");
	if (pObj && pPath)
	{
		pObj->setPosition(pPath->getPointAt(time / 25.0f));
		pObj->setDirection(pPath->getDirectionAt(time / 25.0f));
	}	
	pObj = g_world.getDynamicObject("koraalvlinder1");
	pPath = g_world.findPath("fish4");
	if (pObj && pPath)
	{
		pObj->setPosition(pPath->getPointAt(time / 27.0f));
		pObj->setDirection(pPath->getDirectionAt(time / 27.0f));
	}
	pObj = g_world.getDynamicObject("nemo1");
	pPath = g_world.findPath("fish5");
	if (pObj && pPath)
	{
		pObj->setPosition(pPath->getPointAt(time / 28.0f));
		pObj->setDirection(pPath->getDirectionAt(time / 28.0f));
	}

	// animate seahorse
	if (g_pSeahorsePath)
	{
		Vertex3 point = g_pSeahorsePath->getPointAt(time / 30.0f);
		Vector3 dir = g_pSeahorsePath->getDirectionAt(time / 30.0f);

		seahorse.setPosition(point);
		seahorse.setDirection(dir);
		seahorse.setPhase(time );
	}
}

void idle(void)
{
	// Animate everything
	float time = g_world.getTimer().getTime();
	
	animateAquarium(time);

	glutPostRedisplay();
}

// The mouse callback
void mouse(int button, int state, int x, int y)
{
	g_inputManager.onButton(button, state, x, y);
}

// The motion callback
void activeMotion(int x, int y)
{
	g_inputManager.onMotion(true, x, y);
}

// The passive motion callback
void passiveMotion(int x, int y)
{
	g_inputManager.onMotion(false, x, y);
}

void special(int key, int x, int y)
{
	g_inputManager.onSpecialKey(key, x, y);
}

void specialup(int key, int x, int y)
{
	g_inputManager.onSpecialKeyUp(key, x, y);
}

void keyboard(unsigned char key, int x, int y)
{
	g_inputManager.onKey(key, x, y);
}

void keyboardup(unsigned char key, int x, int y)
{
	g_inputManager.onKeyUp(key, x, y);
}

// Window reshape callback
void reshape(GLsizei w, GLsizei h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glFrustum(-1.0, 1.0, -1.0, 1.0, 1.0, 100.0);
	glMatrixMode(GL_MODELVIEW);	
}

bool initWorld()
{
	cout << "Loading (this can take a while)..." << endl;
	if (!g_world.load(WORLD_FILE)) 
	{
		cerr << "Failed to load .cgworld file. Press a key to quit." << endl;
		system("pause>nul");
		return false;
	}

	g_world.init();
	grass.Load(GRASS_FILE);
	grass.rot.y = 20.0f;
	g_world.getCamera()->setPosition(CAMERA_POS);
	g_world.getCamera()->pitch(CAMERA_PITCH);
	g_world.getCamera()->yaw(CAMERA_YAW);

	g_world.getTimer().start();


	// Get paths and dynamic objects from world
	g_pSeahorsePath =  g_world.findPath("fish4");
	return true;
}


int main(int argc, char **argv)
{

	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize (800,600);
	glutInitWindowPosition(0,0);
    glutCreateWindow (WINDOW_TITLE);

	if (!initWorld())
		return 1;
	
	glEnable(GL_CULL_FACE);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_DEPTH_TEST);

	initAquariumSceneDL();

	glutReshapeFunc(reshape);
	glutMouseFunc(mouse);
	glutMotionFunc(activeMotion);
	glutPassiveMotionFunc(passiveMotion);
	glutIgnoreKeyRepeat(1);
	glutKeyboardFunc(keyboard);
	glutKeyboardUpFunc(keyboardup);
	glutSpecialFunc(special);
	glutSpecialUpFunc (specialup);
	glutIdleFunc(idle);
	glutDisplayFunc(display);

	glutMainLoop();
	return 0;
}

