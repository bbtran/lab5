#include "../external/3DSLoader/Model_3DS.h"
#include "Seahorse.h"
#include "util.h"

Seahorse::Seahorse(): _model(Model_3DS()), _phase(0.0), _position(Vertex3(520,-35,390)), _direction(Vector3(-1,0,1))
{
	_model.Load("../models/seahorse.3ds");
}

Seahorse::~Seahorse()
{
}

void Seahorse::draw()
{
	const float PI = 3.14159265358979324f;
	glPushMatrix();

		// Move dino to position
		glTranslatef(_position.x(), _position.y(), _position.z());

		// Center the dino at its position
		glTranslatef(-8, 0, 0);


		// Rotate the dinosaur into the right direction
		float angle = rad2deg(atan2(-_direction.z(), _direction.x()));
		rotateAround(8.0, 0.0,0, 0, 1, 0, angle);	

		// Move z-axis to the middle of the dino
		glTranslatef(0,0,-1.5);
			
		// Calculate some cycle values
		float	cosVal  =  cos(_phase * 2 * PI); // cosine function
		float   sinVal  =  sin(_phase * 2 * PI); // sine function
		float	cosVal2	=  cos((_phase+0.5) * 2 * PI); // second cosine function

		// Make the whole dino waggle a bit
		rotateAround(7.5, 0,   0, 0, 1, 0,  cosVal * 10);
		rotateAround(0,   1.5, 0, 1, 0, 0, -sinVal * 10);

		glScalef(0.002f, 0.002f, 0.002f);
		_model.Draw();
	glPopMatrix();
}

void Seahorse::rotateAround(float cx, float cy, float cz,
				  float ax, float ay, float az,
				  float angle)
{

	glTranslatef(cx, cy, cz);
	glRotatef(angle, ax, ay, az);
	glTranslatef(-cx, -cy, -cz);
}

float Seahorse::interpolate(float s, float e, float v, float d)
{
	// Interpolates sort of smoothly
	v = v / d;

	v = v * 3.14;
	v = v - 3.14/2;
	v = sin(v);
	v += 1.0;
	v /= 2.0;
	return v * (e-s) + s;
}

void Seahorse::setPosition(Vertex3 pos)
{
	_position = pos;
}

void Seahorse::setDirection(Vector3 dir)
{
	_direction = dir;
}

void Seahorse::setPhase(float phase)
{
	_phase = phase;
}