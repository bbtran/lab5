/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#ifndef _CG_PLANECLIPPER_
#define _CG_PLANECLIPPER_

#include <vector>
#include "Plane.h"
#include "BoundingBox.h"

// A plane clipper contains a set of planes. It can tell whether a bounding box is
// (partially) inside the planes.
class PlaneClipper
{
public:
	PlaneClipper()
	{

	}

	// Adds a plane to the clipper
	void addPlane(const Plane &plane)
	{
		_planes.push_back(plane);
	}
	
	// Returns true if the bounding box is (partially) inside the planes. 
	// This function is conservative, it might say a box is inside while 
	// it's not but that is okay for most purposes.
	bool boxInside(const BoundingBox &box) const
	{
		int planeCount = _planes.size();
	 	for (int i=0;i<planeCount;i++)
		{
			if (!box.insidePlane(_planes[i]))
				return false;
		}
		return true;
	}

private:
	std::vector<Plane> _planes;
};

#endif