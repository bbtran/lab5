/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#ifndef _CG_CGDYNAMIC_OBJECT_H_
#define _CG_CGDYNAMIC_OBJECT_H_

#include "CGObject.h"
#include <string>
#include "BoundingBox.h"
#include "Vector.h"

const int CGDYNAMICOBJECT_MAX_NAME_LEN = 100;

class CGWorld;
class NodeSelector;

// A dynamic world object
class CGDynamicObject : public CGObject
{
public:
	CGDynamicObject(void);
	virtual ~CGDynamicObject(void);

	std::string getName(void) const { return _name; }
	void setName(std::string name) { _name = name; }

	// Draws the dynamic object
	void draw(CGWorld *pWorld, const NodeSelector *pNodeSel);

	// (Un)serializes the object
	void exchange(DataFile *pFile);

	// Set position, yaw/pitch/roll angles
	void setPosition(const Vertex3 pos) { _position = pos; }
	void setYaw(float yaw) { _yaw = yaw; }
	void setPitch(float pitch) { _pitch = pitch; }
	void setRoll(float roll) { _roll = roll; }
	
	// Set the bounding box of the object
	void setBoundingBox(const BoundingBox &box) { _box = box; }
	
	/* Make the object point to the specified direction. A yaw and pitch angle
	   is automatically calculated from the direction vector so that the object
	   points into that direction. Roll is always set to zero and it is assumed
	   the object in the cgworld file points to the positive z-direction. */
	void setDirection(const Vector3 &direction);

	// PrepareRender and finishRender implementations, see CGObject
	virtual void prepareRender(FaceRenderer *pRenderer) const;
	virtual void finishRender(FaceRenderer *pRenderer) const;

	// Set texture shift values
	void setTextureShift(float s, float t);

private:
	std::string _name;				// Name of the object
	Vertex3		_position;			// Position 
	float		_yaw;				// Yaw angle
	float		_pitch;				// Pitch angle
	float		_roll;				// Roll angle
	BoundingBox _box;				// Bounding box of the object
	float		_texShift[2];		// Current texture shift ([0]=s,[1]=t)
};


#endif
