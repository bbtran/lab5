/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#include "CGRoom.h"
#include "PlaneClipper.h"

CGRoom::CGRoom(void)
{
	
}

CGRoom::~CGRoom(void)
{

}

void CGRoom::exchange(DataFile *pFile)
{
	pFile->exchange(&_name, CG_ROOM_MAX_NAME_LEN);
	for (int i=0;i<CG_ROOM_WALL_COUNT;i++)
		_walls[i].exchange(pFile);

	_portalIDs.exchange(pFile);

}

void CGRoom::setName(std::string name)
{
	_name = name;
}

std::string CGRoom::getName(void) const
{
	return _name;
}

void CGRoom::setWall(int index, const Plane &plane)
{
	if (index < CG_ROOM_WALL_COUNT)
		_walls[index] = plane;
}

Plane CGRoom::getWall(int index) const
{
	if (index < CG_ROOM_WALL_COUNT)
		return _walls[index];
	return Plane();
}

void CGRoom::setPortalIDs(const int *pIDs, int count)
{
	_portalIDs.alloc(count);
	memcpy (_portalIDs.get(), pIDs, count * sizeof(int));
}

void CGRoom::addToClipper(PlaneClipper *pClipper)
{
	// Simply add all walls to the clipper
	for (int i=0;i<CG_ROOM_WALL_COUNT;i++)
	{
		pClipper->addPlane(_walls[i]);
	}
}