/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#include "InputManager.h"
#include "CGWorld.h"
#include <GL/glut.h>
#include <iostream>
using namespace std;

InputManager::InputManager(CGWorld *pWorld):
	_pWorld(pWorld), _looking(false),
	_lastFrameTime(-1.0), _showStats(false)
{
}

InputManager::~InputManager(void)
{
}

void InputManager::onButton(int button, int state, int x, int y)
{
	// The button an event occured on:
	if (button!=GLUT_LEFT_BUTTON)
		return;
	if (state != GLUT_UP) 
		return;

	// Start or stop looking
	_looking = !_looking;
	if (_looking)
	{
		glutSetCursor(GLUT_CURSOR_NONE);
		_prevX = x;
		_prevY = y;
	}
	else
	{
		glutSetCursor(GLUT_CURSOR_INHERIT);
	}
}

void InputManager::onMotion(bool active, int x, int y)
{
	if (_looking)
	{
		if (_prevX != x || _prevY != y)
		{
			float dx = x - _prevX;
			float dy = y - _prevY;
			
			Camera *pCam = _pWorld->getCamera();


			pCam->yaw(-dx * INPUT_MANAGER_MOUSE_FACTOR); 
			pCam->pitch(-dy * INPUT_MANAGER_MOUSE_FACTOR);

			// Move mouse to center again
			x = glutGet(GLUT_WINDOW_WIDTH) / 2;
			y = glutGet(GLUT_WINDOW_HEIGHT) / 2;
			glutWarpPointer(x, y);
			_prevX = x;
			_prevY = y;
		}
	}
}

void InputManager::onFrame()
{
	Camera *pCam = _pWorld->getCamera();

	// Get the time passed since the last frame
	double time = _pWorld->getTimer().getTime();
	if (_lastFrameTime < 0)
		_lastFrameTime = time;
	double timePassed = time - _lastFrameTime;
	_lastFrameTime = time;

	float walk = 0.0, strafe = 0.0;
	// Update walk and strafe values
	if (_upKey)
	{
		walk += timePassed * INPUT_MANAGER_WALK_SPEED;
	}
	if (_downKey)
	{
		walk -= timePassed * INPUT_MANAGER_WALK_SPEED;
	}

	if (_leftKey)
	{
		strafe -= timePassed * INPUT_MANAGER_WALK_SPEED;
	}
	if (_rightKey)
	{
		strafe += timePassed * INPUT_MANAGER_WALK_SPEED;
	}

	// Update camera
	pCam->update(walk, strafe);
}

void InputManager::onSpecialKey(int key, int x, int y)
{
	switch(key)
	{
	case GLUT_KEY_UP:
		_upKey = true;
		break;
	case GLUT_KEY_DOWN:
		_downKey = true;
		break;
	case GLUT_KEY_LEFT:
		_leftKey = true;
		break;
	case GLUT_KEY_RIGHT:
		_rightKey = true;
		break;
	}
}

void InputManager::onSpecialKeyUp(int key, int x, int y)
{
	switch(key)
	{
	case GLUT_KEY_UP:
		_upKey = false;
		break;
	case GLUT_KEY_DOWN:
		_downKey = false;
		break;
	case GLUT_KEY_LEFT:
		_leftKey = false;
		break;
	case GLUT_KEY_RIGHT:
		_rightKey = false;
		break;
	}
}

void InputManager::onKey(unsigned char key, int x, int y)
{
	
	// Start/stop animation on space bar
	if (key=='\r')
	{
		_showStats = !_showStats;
	}
	if (key==' ')
	{
		Timer &timer =_pWorld->getTimer();
		if (timer.isRunning()) timer.stop();
		else timer.start();
	}
	
	if (key == 'w') _upKey = true;
	if (key == 's') _downKey = true;
	if (key == 'a') _leftKey = true;
	if (key == 'd') _rightKey = true;
}

void InputManager::onKeyUp(unsigned char key, int x, int y)
{
	
	// Start/stop animation on space bar
	if (key=='\r')
	{
		_showStats = !_showStats;
	}
	if (key==' ')
	{
		Timer &timer =_pWorld->getTimer();
		if (timer.isRunning())
			timer.stop();
		else
			timer.start();
	}
	
	if (key == 'w') _upKey = false;
	if (key == 's') _downKey = false;
	if (key == 'a') _leftKey = false;
	if (key == 'd') _rightKey = false;
}