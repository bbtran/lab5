/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#include "CGPath.h"
#include "Vertex.h"

CGPath::CGPath(void)
{
}

CGPath::~CGPath(void)
{
}

void CGPath::exchange(DataFile *pFile)
{
	pFile->exchange(&_name, CG_PATH_MAX_NAME_LEN);
	_points.exchange(pFile);
}


void CGPath::setPoints(const Vertex3 *pVertices, int count)
{
	_points.alloc(count);
	memcpy(_points.get(), pVertices, count * sizeof(Vertex3));
}

void CGPath::setName(std::string name)
{
	_name = name;
}

std::string CGPath::getName() const
{
	return _name;
}

Vertex3 CGPath::getPointAt(float phase) const
{
	int pointCount = _points.size();
	// Change to 0..1 range
	phase = fmod(phase, 1.0f);
	float realIndex = phase * pointCount;
	int idx = static_cast<int>(realIndex);
	float step = realIndex - idx;

	return bezierInterpolate(
				_points[(idx + pointCount - 1)  % pointCount],
				_points[(idx)  % pointCount],
				_points[(idx + 1) % pointCount],
				_points[(idx + 2) % pointCount], step);
}

Vector3 CGPath::getDirectionAt(float phase) const
{
	int pointCount = _points.size();
	// Change to 0..1 range
	phase = fmod(phase, 1.0f);
	float realIndex = phase * pointCount;
	int idx = static_cast<int>(realIndex);
	float step = realIndex - idx;

	// Interpolate between normalized direction vectors
	return bezierInterpolate(
				(_points[(idx )  % pointCount] - _points[(idx + pointCount - 1)  % pointCount]).normalized(),
				(_points[(idx + 1) % pointCount] - _points[(idx)  % pointCount]).normalized(),
				(_points[(idx + 2) % pointCount] - _points[(idx + 1) % pointCount]).normalized(),
				(_points[(idx + 3) % pointCount] - _points[(idx + 2) % pointCount]).normalized()
				, step).normalized();
}
