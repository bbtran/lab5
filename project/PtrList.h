/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#ifndef _CG_PTRLIST_H_
#define _CG_PTRLIST_H_

#include <vector>
#include "DisallowCopy.h"
#include "DataFile.h"

// Maximum list element count (safety value to prevent
// large list allocation).
const int PTR_LIST_MAX_ELEMENT_COUNT = 1000*5;

/*	This class can contain a dynamic array of object pointers. 
	All	object pointers are automatically deleted when this 
	object is destroyed. */
template<typename T>
class PtrList : private DisallowCopy
{
public:

	// Create a new, empty pointer list
	PtrList()
	{
	}

	// Add a pointer to the list
	int add(T *p)
	{
		int idx = size();
		_list.push_back(p);
		return idx;
	}

	// Clear the list. This will delete all objects pointed to as well.
	void clear()
	{
		int count = size();
		for (int i=0;i<count;i++)
		{
			delete(_list[i]);
		}	
	}

	// Destroy the list and all the objects pointed to by the list
	~PtrList(void)
	{
		clear();
	}

	// (Un)serialize all pointers *and* all objects in the list.
	void exchange(DataFile *pFile)
	{
		// Read or write count
		int count = static_cast<int>(_list.size());
		pFile->exchange(&count);

		
		if (pFile->writing())
		{
			// Write all objects to the file
            for (int i=0;i<count;i++)
				_list[i]->exchange(pFile);
		}
		else
		{
			// Check for a sensible element count
			if (count < 0 || count > PTR_LIST_MAX_ELEMENT_COUNT)
			{
				throw DataFile::Error("Too many elements in pointer list");
			}

			for (int i=0;i<count;i++)
			{
				// Allocate a new object
				T *pObj = new T();
				// Add it to the list
				add(pObj);
				// Read the object's data
				pObj->exchange(pFile);
			}
		}
	}

	// Index operator
	T * operator[](int i) { return _list[i]; }

	// Const index operator
	const T * operator[] (int i) const { return _list[i]; }

	// Returns the number of pointers stored
	int size() const { return static_cast<int>(_list.size()); }

private:
	std::vector<T*> _list;	// The vector to store the pointers
};

#endif
