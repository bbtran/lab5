/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#ifndef _CG_CGFACEID_H_
#define _CG_CGFACEID_H_


// Simple flat structure storing an object and face index
struct CGFaceID
{
	unsigned short objectIdx;
	unsigned short faceIdx;
};

#endif
