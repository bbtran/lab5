/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#pragma warning (disable: 4786)
#include "CGFaceTreeIterator.h"

#include "CGFaceTree.h"
#include "CGWorld.h"
#include "Triangle.h"
#include "NodeSelector.h"

CGFaceTreeIterator::CGFaceTreeIterator(const CGWorld *pWorld, NodeSelector *pSelector):
	_pSelector(pSelector), _pWorld(pWorld), _pTree(pWorld->getFaceTree()),
	_state(INIT_FACES), _curNodeIndex(0), _pFaceIDs(NULL), _facesLeft(0),
	_nodeCount(_pTree->getNodeCount())
{
}

CGFaceTreeIterator::~CGFaceTreeIterator(void)
{
}

bool CGFaceTreeIterator::next()
{
	while(true)
	{
		// Are we done yet?
		if (_curNodeIndex == 0 && _state == BACK)
			return false; // done

		// Invalid node index? go back
		if (_curNodeIndex >= _nodeCount)
			_state = BACK;

		// Get the current node
		CGFaceTreeNode node = _pTree->getNode(_curNodeIndex);

		switch(_state)
		{
		case INIT_FACES:
			{
				// If node is not selected, ignore it and go back to its parent
				if (!_pSelector->includes(node.box))
				{
					_state = BACK; // done, no recursion
					break;
				}
				
				// Load faces of current node
				int count = node.faceCount;
				if (count <= 0)
				{
					// Not a leaf node, recurse into its left child
					_state = GO_LEFT;
					break;
				}

				// Setup pointer to face IDs and the number of faces to iterate 
				_facesLeft = count;
				_pFaceIDs = _pTree->getFaceIDPtr(node.faceIDIndex);
				_state = RETURN_FACES;
			}
			break;

		case RETURN_FACES:
			// All faces done? recurse back
			if (_facesLeft==0)
			{
				_state = BACK;
				break;
			}
			
			// Get a new triangle and set the current triangle
			_curFaceID = *_pFaceIDs;
			_curTriangle = _pWorld->getTriangle(_curFaceID);
			_pFaceIDs++;
			_facesLeft--;
			return true;
		
		case GO_LEFT:
			// Recurse to the left child (index 2i+1)
			_curNodeIndex = _curNodeIndex * 2 + 1;
			_state = INIT_FACES;
			break;

		case GO_RIGHT:
			// Recurse to the right child (index 2i+2)
			_curNodeIndex = _curNodeIndex * 2 + 2;
			_state = INIT_FACES;
			break;

		case BACK:
			/* Return from recursion to the parent. In order to do this,
			   we need to know if we are in the left or the right child of 
			   the current parent. */
			{
				// left child indices are always odd, right child indices are even
				bool isLeftPath = ((_curNodeIndex - 1) % 2) == 0;
				// Get parent:
				_curNodeIndex = (_curNodeIndex - 1) / 2;
				
				// If we visited the left child, we still need to visit the right one, if not
				// then we are done and we can go back even further.
				if (isLeftPath)
					_state = GO_RIGHT;
				else
					_state = BACK;
			}
			break;
		}
	}
}

const Triangle & CGFaceTreeIterator::current() 
{
	return _curTriangle;
}

CGFaceID CGFaceTreeIterator::currentFace() const
{
	return _curFaceID;
}

