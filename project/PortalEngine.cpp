/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#include "PortalEngine.h"
#include "CGWorld.h"
#include "CGRoom.h"
#include "Statistics.h"

PortalEngine::PortalEngine(CGWorld *pWorld): _pWorld(pWorld)
{
}

PortalEngine::~PortalEngine(void)
{
}


PortalNodeSelector PortalEngine::getSelector(const Frustum &frustum)
{
	// Build a selector
	PortalNodeSelector selector;

	// Set the frustum
	selector.setFrustum(frustum);
	Vertex3 cameraPos = _pWorld->getCamera()->getPosition();
	
	// visitedPortals is an integer array with the IDs of the portals that have
	// already been visisted. 
	AutoArr<int> visitedPortals;
	visitedPortals.alloc(_pWorld->getPortalCount());
	
	// Check all rooms
	int roomCount = _pWorld->getRoomCount();
	for (int i=0;i<roomCount;i++)
	{
		// Are we inside this room?
		CGRoom *pRoom = _pWorld->getRoom(i);
		if (pRoom->isInside(cameraPos))
		{
			Statistics::instance().addInRoom(pRoom->getName());
			// Test other rooms for visibility recursively
			PlaneClipper emptyClipper;
			recurseRooms(i, &selector, visitedPortals.get(), 0, cameraPos, emptyClipper, frustum);
		}
	}
	return selector;
}

void PortalEngine::recurseRooms(int roomID,			// Current room ID
				  PortalNodeSelector *pSelector,	// Selector to be built
				  int *pVisitedPortals,				// Pointer to list of portal IDs already visited
				  int visitedPortalsCount,			// Number of items in pVisitedPortals
				  const Vertex3 &cameraPos,			// Camera position
				  PlaneClipper visiblePart,			// Part of the current room visible
				  const Frustum &frustum)			// Current frustum
{
	
	// Add the room's walls to the current clipper
	PlaneClipper curRoomClipper = visiblePart;
	
	CGRoom *pRoom = _pWorld->getRoom(roomID);
	Statistics::instance().addVisibleRoom(pRoom->getName());
	pRoom->addToClipper(&curRoomClipper);
	pSelector->addClipper(curRoomClipper);

	// Check all portals
	int portalCount = pRoom->getPortalCount();
	for (int i=0;i<portalCount;i++)
	{
		int portalID = pRoom->getPortalID(i);

		// Did we visit this portal already?
		bool portalVisited = false;
		for (int k=0;k<visitedPortalsCount;k++)
		{
			if (pVisitedPortals[k]==portalID)
			{
				portalVisited = true;
				break;
			}
		}

		// if not visited yet:
		if (!portalVisited)
		{
			// Add to visited list
			pVisitedPortals[visitedPortalsCount] = portalID;
			CGPortal *pPortal = _pWorld->getPortal(portalID);
			
			// Get the ID of the room we can see through the portal
			int firstRoomID = pPortal->getRoomID(0);
			int secondRoomID = pPortal->getRoomID(1);
			int nextRoomID = (firstRoomID == roomID) ? secondRoomID : firstRoomID;
			
			// Is this portal actually visible? Check frustum first
			if (frustum.boxInFrustum(pPortal->getBoundingBox()))
			{
				//  Is this portal in the current visible part (through other portals)?
				if (visiblePart.boxInside(pPortal->getBoundingBox()))
				{
					// Create a clipper through this portal
					PlaneClipper nextRoomClipper = visiblePart;
					pPortal->addToClipper(&nextRoomClipper, cameraPos);

					// Recurse through this portal to other rooms
					recurseRooms(nextRoomID, pSelector, pVisitedPortals, visitedPortalsCount + 1,
								 cameraPos, nextRoomClipper, frustum);
					
				}
			}
		}
	}
}