/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#ifndef _CG_CGFACETREEITERATOR_
#define _CG_CGFACETREEITERATOR_

#include "TriangleIterator.h"
#include "CGFaceID.h"
#include "Triangle.h"

class CGFaceTree;
class CGWorld;
class NodeSelector;

// Iterates over triangles from a face tree
class CGFaceTreeIterator : public TriangleIterator
{
public:

	// Create a new iterator
	CGFaceTreeIterator(const CGWorld *pWorld, NodeSelector *pSelector);
	virtual ~CGFaceTreeIterator(void);
	
	// Implements TriangleIterator
	virtual bool next();
	virtual const Triangle & current();

	// Gets the face ID of the current triangle
	CGFaceID currentFace() const;

private:
	const CGFaceTree *_pTree;		// Pointer to the face tree
	const CGWorld	*_pWorld;		// Pointer to the world
	
	// States for the iteration function
	enum States
	{
		INIT_FACES,		// Load the faces of a node
		RETURN_FACES,	// Return the faces to the user
		GO_LEFT,		// Recurse into the left child
		GO_RIGHT,		// Recurse into the right child
		BACK			// Return from recursion to the parent
	};

	States			_state;				// Current state
	int				_curNodeIndex;		// Current node index
	const CGFaceID *_pFaceIDs;			// Pointer to the current face ID list
	CGFaceID		_curFaceID;			// Current face ID
	int				_facesLeft;			// Number of faces to list yet in the current face ID list
	Triangle		_curTriangle;		// Current triangle
	int				_nodeCount;			// Number of nodes
	NodeSelector	*_pSelector;		// Pointer to the node selector


};

#endif
