/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#ifndef _CG_PORTALNODESELECTOR_H_
#define _CG_PORTALNODESELECTOR_H_

#include "Frustum.h"

// Selects node based on a frustum and a set of PlaneClippers.
// If a node is in the frustum or in one of the PlaneClippers, it will select the node
class PortalNodeSelector : public NodeSelector
{
private:
	typedef std::vector<PlaneClipper> Clippers;
public:
	PortalNodeSelector::PortalNodeSelector()
	{
		_frustumUsed = false;
	}
	virtual ~PortalNodeSelector(void) {}

	
	virtual bool PortalNodeSelector::includes(const BoundingBox &node) const
	{
		// Check if node is in frustum
		if (_frustumUsed && !_frustum.boxInFrustum(node))
			return false;

		// Check if it is in any of the clippers
		Clippers::const_iterator it;
		for (it=_clippers.begin();it!=_clippers.end();it++)
		{
			if (it->boxInside(node))
				return true;
		}
		return false;
	}

	void PortalNodeSelector::addClipper(const PlaneClipper &clip)
	{
		_clippers.push_back(clip);
	}

	void PortalNodeSelector::setFrustum(const Frustum &frustum)
	{
		_frustum = frustum;
		_frustumUsed = true;
	}

private:
	

	Clippers	_clippers;
	Frustum		_frustum;
	bool		_frustumUsed;
};







#endif

