/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#ifndef _CG_CGROOM_H_
#define _CG_CGROOM_H_

#include "DataFile.h"
#include "Plane.h"
#include <string>
#include "DisallowCopy.h"
#include "AutoArr.h"

const int CG_ROOM_WALL_COUNT = 4;
const int CG_ROOM_MAX_NAME_LEN = 1000;

class PlaneClipper;

// A room for the portal engine
class CGRoom : private DisallowCopy
{
public:
	CGRoom(void);
	virtual ~CGRoom(void);
	void exchange(DataFile *pFile);

	// Set/get name
	void setName(std::string name);
	std::string getName(void) const;

	// Set/get the walls of the room
	void setWall(int index, const Plane &plane);
	Plane getWall(int index) const;
	int getWallCount(void) const { return CG_ROOM_WALL_COUNT; }

	// Get/set the portals of this room
	void setPortalIDs(const int *pIDs, int count);
	int getPortalCount() const { return _portalIDs.size(); }
	int getPortalID(int index) const { return _portalIDs[index]; }

	// Add the room to a plane clipper
	void addToClipper(PlaneClipper *pClipper);

	// Tells whether a point is inside this room
	bool isInside(const Vertex3 &point)
	{
		// The point is inside if it is not outside
		// any of its planes.
		for (int i=0;i<CG_ROOM_WALL_COUNT;i++)
		{
			if (_walls[i].side(point) < 0)
				return false;
		}
		return true;
	}

private:
	// The walls of the room
	Plane			_walls[CG_ROOM_WALL_COUNT];

	// Array of portal IDs
	AutoArr<int>	_portalIDs;
	
	// Name of the room
	std::string		_name;
};

#endif
