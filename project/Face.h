/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#ifndef _CG_FACE_H_
#define _CG_FACE_H_

#include "Vertex.h"
#include "Vector.h"

/* Flat structure that contains information about a face (a triangle with normals etc.) */
struct Face
{
	Vertex3	points[3];		// The face's vertices
	Vector3 normals[3];		// The normal of each vertex

	float	tex[3][2];		// Texture coordinates for each vertex
	float	lightTex[3][2];	// Lightmap texture coordinates for each vertex
};


#endif
