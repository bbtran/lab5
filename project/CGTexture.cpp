/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#include "cgtexture.h"
#include <util/glutil.c>
#include <GL/glut.h>
using namespace std;

CGTexture::CGTexture(): _glTexName(-1)
{
}

CGTexture::CGTexture(const char *pFilename)
{
	_glTexName = -1;
	_filename = pFilename;
}

CGTexture::~CGTexture()
{
}

void CGTexture::exchange(DataFile *pFile)
{
	pFile->exchange(&_filename, MAX_FILE_LEN);
}

std::string CGTexture::getFilename() const
{
	return _filename;
}

void CGTexture::activate() const
{
	if (_glTexName==-1) return;
	glBindTexture(GL_TEXTURE_2D, _glTexName);
}

bool CGTexture::loadTextureFile()
{
	// Already loaded?
	if (_glTexName!=-1) return true;

	// Load RGB file
	RGBImage *pImage = LoadRGB(_filename.c_str());
	if (!pImage) return false;

	// Create a new texture name
	glGenTextures(1, &_glTexName);
	
	// Bind the texture and set some parameters
	glBindTexture(GL_TEXTURE_2D, _glTexName);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	// Build mipmaps automatically
	int ret = gluBuild2DMipmaps(GL_TEXTURE_2D, pImage->components, 
								pImage->sizeX, pImage->sizeY,
								pImage->format, 
								GL_UNSIGNED_BYTE, pImage->data);

	free(pImage); 

	// Building failed
	if (ret!=0) return false;

	// Success!
	return true;
}
