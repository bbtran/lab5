#ifndef _SEAHORSE_H_
#define _SEAHORSE_H_

#include "Vertex.h"
#include "Vector.h"
#include "../external/3DSLoader/Model_3DS.h"

class Seahorse
{
public:
	Seahorse();
	void draw();
	virtual ~Seahorse();

	// Set position, direction and phase (in swimming)
	void setPosition(Vertex3 pos);
	void setDirection(Vector3 dir);
	void setPhase(float phase); // phase is 0..1 of the swimming cycle

private:
	// Rotate around a given point
	static void rotateAround(float cx, float cy, float cz,		// The point to rotate around
							 float ax, float ay, float az,		// The rotation vector
							 float angle);						// The rotation angle

	// Smoothly interpolates between s and e at point (v/d)
	static float interpolate(float s, float e, float v, float d);

	float		_phase;			// Phase of the swimming cycle
	Vertex3		_position;
	Vector3		_direction;
	Model_3DS   _model;
};

#endif