/*
	(C) 2004 - 2008 Thomas Bleeker
	MadWizard.org
	
	Released under the BSD license with the exception of parts of the code 
	not written by Thomas Bleeker (www.madwizard.org), which are marked in
	the source code. See license.txt for details.
*/

#ifndef _CG_COLLISIONTESTER_H_
#define _CG_COLLISIONTESTER_H_

#include "3dclasses.h"

class CGWorld;

const float COLLISION_EPSILON = 1.0e-4f;
const int COLLISION_MAX_STEPS = 6;
// Class to test for collision
class CollisionTester 
{
public:
	// Create a new teset. rx, ry and rz are the ellipsoid radius in each dimension
	CollisionTester(CGWorld *pWorld, float rx, float ry, float rz);
	virtual ~CollisionTester(void);

	// Test for collision from source along velocity. Returns a new point
	Vertex3 collide(Vertex3 source, Vector3 velocity);

	// Get the number of triangles tested in the last test
	int getLastTriangleCount() const;
private:
	struct Collision;
	CGWorld	*_pWorld;
	float	 _scaleX, _scaleY, _scaleZ;	// The ellipsoid scaling factors

	// Returns the lowest possitive solution of a*x^2 + bx + c = 0 in x.
	// Returns true if there is a solution, false otherwise
	bool getLowestPositiveRoot(float a, float b, float c, float *x);
	
	// Sweeps the sphere against a single point
	inline bool sweepSinglePoint(const Vertex3 &sourcePoint,
								 const Vector3 &velocity,
								 const Vertex3 &testPoint,
								 Vertex3 *pCollisionPoint,
								 float *pT);

	// Sweeps the sphere against all triangle points
	bool sweepPoints(const Vertex3 &sourcePoint,
					 const Vector3 &velocity, 
					 const Triangle &poly, 
					 Vertex3 *pCollisionPoint, 
					 float *pT);
	
	// Sweeps the sphere against a single edge
	bool sweepSingleEdge(const Vertex3 &sourcePoint,
					     const Vector3 &velocity,
					     const Ray &edge,
						 Vertex3 *pCollisionPoint,
						 float *pT);

	// Sweeps the sphere against all triangle edges
	bool sweepEdges(const Vertex3 &sourcePoint,
					const Vector3 &velocity, 
					const Triangle &poly, 
					Vertex3 *pCollisionPoint, 
					float *pT);

	// Test for a collsiion
	void testCollision(Collision *pCol);

	int		_lastTriangleCount;

	// Structure to store some collision information
	struct Collision
	{
		Vertex3		sourcePoint;			// The starting point
		Vector3		velocity;				// The vector to travel

		bool		collisionFound;			// True if a collision was found
		float		nearestDistance;		// Distance to nearest collision
		Vertex3		intersectionPoint;		// Intersection point of nearest collision
		
		bool		stuck;					// We are stuck
		BoundingBox	boundingBox;			// Bounding box to check collisions within
	};

	Vertex3 _lastValidPosition;		// The last position we were not stuck
};


// Utility template function to swap two variables
template<class T> inline void swapVars(T &a, T &b)
{
	T temp;
	temp = b;
	b = a;
	a = temp;
};

// Utility function to clamp a variable between 0 and 1
inline void clamp01(double &f)
{
	if (f < 0.0) 
		f = 0.0;
	else if (f > 1.0) 
		f = 1.0;
}

#endif
